# XinMedia-XinDesign #

 使用時須將專案中的 babel 設定檔設定 如何按需加載，可以參考 resources/babel/ 資料夾中的 .babelrc.js 的設定方式。

## 下列為 Lerna 的使用方法 ##

```code
yarn global add lerna verdaccio nrm
```

設定 package 為獨立版本

```code
lerna init --independent
```

查看旗下所要 package

```code
lerna list
```

建立新的 package (有自建 commander 參考如下)

```code
lerna create [PackageName] -y
```

依賴關係

```code
lerna add [basicPackageName] --scope [PackageName]
```

安裝好所有依賴關係

```code
lerna bootstrap --hoist
```

清除所有 node_modules

```code
lerna clean
```

產生版本號後自動 PUSH 到 REOMTE GIT REPO

```code
lerna version
```

本地引用 package 需到 package的component下 (/packages/components) run yarn link 後( ~/.config/yarn/link )，即可在專案下引用

```code
yarn link
```

執行 packages 下所有 package 的 build 方法

```code
lerna run build
```
