declare module '*.md' {
    const content: string;
    export default content;
}

declare module '*.mdx' {
    const content: any;
    export default content;
}

declare module '*.json' {
    const content: string;
    export default content;
}

declare module '*.scss' {
    // const styles: { [className: string]: string };
    // export default styles;
    interface IClassNames {
        [className: string]: string;
    }
    const classNames: IClassNames;
    export = classNames;
}

declare module '*.svg' {
    interface ISvgParm {
        [className: string]: any;
    }
    const content: ISvgParm;
    export = content;
}
