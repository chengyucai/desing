import styled from 'styled-components';

export const StyledPagination = styled.ul`
    display: flex;
    flex-direction: row;
    list-style: none;
    font-family: Microsoft JhengHei;

    &.disabled {
        cursor: not-allowed;
    }
`;

const item = () => `
    display: inline-block;
    min-width: 32px;
    height: 32px;
    margin-right: 8px;
    line-height: 32px;
    text-align: center;
    vertical-align: middle;
    list-style: none;
    background-color: #fff;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    outline: 0;
    cursor: pointer;
    user-select: none;
    &.active {
        font-weight: 500;
        background: #fff;
        border-color: #1890ff;
        color: #1890ff;
    }
    > a {
        display: block;
        padding: 0 6px;
        color: rgba(0, 0, 0, 0.65);
        -webkit-transition: none;
        transition: none;
        text-decoration: none;
    }

    &.next{
        .xin-icon{
            height: 32px;
            width: 32px;
            display: block;
            transform: rotate(-90deg);
        }
    }
    &.prev{
        .xin-icon{
            height: 32px;
            width: 32px;
            display: block;
            transform: rotate(90deg);
        }
    }

    &.disabled {
        cursor: not-allowed;
        background: #d9d9d9;
    }`;

export const StyledPageStyle = styled.li<{ type: 'page' | 'prev' | 'next' }>`
    ${item}
`;

export const StyledEllipsis = styled.li`
    ${item}
    display: block;
    color: rgba(0, 0, 0, 0.25);
    border: 0;
    letter-spacing: 2px;
`;

export const StyledOption = styled.li`
    display: inline-block;
    margin-left: 16px;
    vertical-align: middle;
`;

export const StyledJumperStyle = styled.div`
    display: inline-block;
    height: 32px;
    line-height: 32px;
    vertical-align: top;
    color: rgba(0, 0, 0, 0.65);

    > input {
        position: relative;
        display: inline-block;
        height: 32px;
        padding: 4px 11px;
        color: rgba(0, 0, 0, 0.65);
        font-size: 14px;
        line-height: 1.5;
        background-color: #fff;
        background-image: none;
        border: 1px solid #d9d9d9;
        border-radius: 4px;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        width: 50px;
        margin: 0 8px;
        box-sizing: border-box;
    }
`;
