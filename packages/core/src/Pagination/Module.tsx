import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledPagination, StyledJumperStyle, StyledPageStyle, StyledOption, StyledEllipsis } from './scss/Style';
import Icon from '../Icon';

export interface IPaginationProps extends ICommonProps {
    current?: number;
    onChange?: (page: number) => void;
    itemRender?: (type: 'page' | 'prev' | 'next', page?: number) => React.ReactNode;
    disabled?: boolean;
    showQuickJumper?: boolean;
    jumperRender?: (pageChange: (page: number) => void) => React.ReactNode;
    pageSize?: number;
    total?: number;
}

export const Module: React.FC<IPaginationProps> = props => {
    const { total, pageSize, current = 1, onChange, disabled = false, itemRender, showQuickJumper = false, jumperRender } = props;

    const handleChange = (page: number) => {
        if (page >= 1 && page <= Math.ceil(total / pageSize)) !disabled && onChange && onChange(page);
    };

    const renderPages = () => {
        const totalPage = Math.ceil(total / pageSize);
        let pages: (number | string)[] = [];

        if (current < 5) {
            pages = [1, 2, 3, 4, 5, 'ellipsis', totalPage];
        } else if (current > totalPage - 4) {
            pages = [1, 'ellipsis', totalPage - 4, totalPage - 3, totalPage - 2, totalPage - 1, totalPage];
        } else {
            pages = [1, 'ellipsis', current - 2, current - 1, current, current + 1, current + 2, 'ellipsis', totalPage];
        }

        if (totalPage <= 5) {
            pages = [];
            for (let i = 1; i <= totalPage; i++) pages.push(i);
        }

        return pages.map((page, idx) => {
            if (typeof page === 'number') {
                return (
                    <Page key={`page_${idx}`} type="page" active={current === page} disabled={disabled} itemRender={itemRender && itemRender('page', page)} clickCallback={() => handleChange(page)}>
                        {page}
                    </Page>
                );
            } else {
                return (
                    <StyledEllipsis key={idx} className={cx('ellipsis', { disabled: disabled })}>
                        <span>•••</span>
                    </StyledEllipsis>
                );
            }
        });
    };

    const renderJumper = () => {
        return <StyledOption className="option">{jumperRender ? jumperRender(handleChange) : <Jumper pageChange={handleChange} />}</StyledOption>;
    };

    return (
        <StyledPagination className={cx('Pagination', 'ld-pagination', props.className)}>
            <Page type="prev" disabled={disabled || current === 1} itemRender={itemRender && itemRender('prev')} clickCallback={() => handleChange(current - 1)}>
                <Icon type="arrow-down" fontSize={32} />
            </Page>
            {renderPages()}
            <Page type="next" disabled={disabled || current === Math.ceil(total / pageSize)} itemRender={itemRender && itemRender('next')} clickCallback={() => handleChange(current + 1)}>
                <Icon type="arrow-down" fontSize={32} />
            </Page>
            {showQuickJumper && renderJumper()}
        </StyledPagination>
    );
};

interface IJumperProps {
    pageChange: (page: number) => void;
}

const Jumper: React.FC<IJumperProps> = props => {
    const { pageChange } = props;
    const [input, setInput] = React.useState<string>('');
    React.useEffect(() => {
        const handleEnter = (e: KeyboardEvent) => {
            if (e.keyCode === 13) {
                pageChange(Number(input));
                setInput('');
            }
        };
        window.addEventListener('keypress', handleEnter);
        return () => {
            window.removeEventListener('keypress', handleEnter);
        };
    }, [input]);

    return (
        <StyledJumperStyle className="Jumper">
            跳至
            <input type="text" value={input} onChange={e => setInput(e.target.value)} />頁
        </StyledJumperStyle>
    );
};

interface IPageProps {
    type: 'page' | 'prev' | 'next';
    active?: boolean;
    disabled?: boolean;
    itemRender?: React.ReactNode;
    clickCallback: () => void;
}

const Page: React.FC<IPageProps> = React.memo(
    props => {
        const { type, children, active = false, clickCallback, disabled, itemRender } = props;
        return (
            <StyledPageStyle type={type} className={cx('page', { active: active, disabled: disabled, prev: type === 'prev', next: type === 'next' })} onClick={clickCallback}>
                {itemRender ? itemRender : <a>{children || type}</a>}
            </StyledPageStyle>
        );
    },
    (prevProps, nextProps) => {
        let reRender = false;
        if (prevProps.active === nextProps.active) reRender = true;
        else reRender = false;
        if (prevProps.clickCallback === nextProps.clickCallback) reRender = true;
        else reRender = false;
        return reRender;
    },
);

export default Module;
