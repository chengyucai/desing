import styled from 'styled-components';

export const StyledBadge = styled.div`
    position: relative;
    display: inline-block;
`;

const _Box = (font: number) => {
    const box = (font * 3) / 2;
    return `
            min-width: ${font / 2}px;
            height: ${box}px;
            padding: 0 ${font / 2}px;
            font-size: ${font}px;
            border-radius: ${box}px;
            line-height: ${box}px;`;
};

interface IBadge_P {
    font: number;
    color: string;
}

export const StyledBadgeCount = styled.div`
    ${(props: IBadge_P) => _Box(props.font)};

    position: absolute;
    top: 0;
    right: 0;
    transform-origin: 100% 0%;
    transform: translate(50%, -50%);
    background-color: #aaaa;
    color: #fff;
    font-weight: normal;
    white-space: nowrap;
    text-align: center;
    background: #f5222d;
    z-index: 10;
    box-shadow: 0 0 0 1px #fff;

    &.dot {
        ${_Box(6)};
        color: transparent;
        box-sizing: content-box;
    }

    &:not(.Show) {
        display: none;
    }

    &::after {
        content: '';
    }
`;
