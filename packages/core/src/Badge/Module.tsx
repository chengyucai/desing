import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledBadge, StyledBadgeCount } from './scss/style';

export interface IBadgeProps extends ICommonProps {
    onCallback?: () => void;
    count?: number;
    overflowCount?: number;
    font?: number;
    dot?: boolean;
    showZero?: boolean;
    color?: string;
    status?: 'processing';
    inStyle?: React.CSSProperties;
}

const Module: React.FC<IBadgeProps> = props => {
    const { onCallback, count, children, overflowCount, font, dot, showZero, color } = props;

    const Show = count !== undefined && (showZero && !dot ? count >= 0 : count > 0);

    const _Limit = (count?: number) => {
        if (dot || count === undefined) return '';
        if (showZero && count === 0) return '0';

        return overflowCount < count ? `${overflowCount}+` : count;
    };

    return (
        <StyledBadge className={cx('Badge', 'ld-badge', props.className)} onClick={() => onCallback && onCallback()}>
            <StyledBadgeCount className={cx('badgeCount', { Show: Show }, { dot: dot })} color={color} font={font}>
                {_Limit(count)}
            </StyledBadgeCount>
            {children}
        </StyledBadge>
    );
};

Module.defaultProps = { overflowCount: 99, font: 12 };

export default Module;
