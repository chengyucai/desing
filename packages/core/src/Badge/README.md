# Badge

圖示右上角的圓形圖標數字。

## 何時使用

一般出現在通知圖標或頭像的右上角，用於顯示需要處理的消息條數，通過醒目視覺形式吸引用戶處理。

## `API`

```jsx
<Badge count={5}>
  <a href="#" className="example" />
</Badge>
```

```jsx
<Badge count={5} />
```

Property    | Description     |Type              | Default  |
----------  |----------       |------------      |----------|
| color     | 自定義小圓點的顏色 | string           | - |
| font      | 自定義Badge的大小 | string            | - |
| count     | 展示的數字，大於overflowCount時顯示為${overflowCount}+，為0時隱藏 | ReactNode |  |
| dot       | 	不展示數字，只有一個小紅點          | boolean | false |
| overflowCount | 展示封頂的數字值 | number         | 99 |
| showZero  | 	當數值為0時，是否展示Badge | boolean | false |
| status    | 設置Badge為狀態點 | Enum{ 'processing' } | '' |