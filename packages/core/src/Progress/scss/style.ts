import styled from 'styled-components';
interface IProgressProps {
    percent: number;
}

const _text = () => `
    margin-left: 8px;
    min-width: 38px;
    font-size: 100%;`;

export const StyledProgress = styled.div<IProgressProps>`
    position: relative;
    display: flex;
    align-items: center;

    &.line {
        width: 100%;

        > div.Progress-inner {
            position: relative;
            display: inline-block;
            width: 100%;
            overflow: hidden;
            vertical-align: middle;
            background-color: #f5f5f5;
            border-radius: 100px;
            height: 6px;

            > div.Progress-bg {
                position: relative;
                background-color: #1890ff;
                border-radius: 100px;
                height: inherit;
                transition: width 0.3s;
                &.exception {
                    background-color: #d0104c;
                }
                &.fill {
                    background-color: #86c166;
                }

                width: ${props => `${props.percent}%`};
                &.active::before {
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    background: #fff;
                    border-radius: 10px;
                    opacity: 0;
                    animation: Progress-active 2.4s cubic-bezier(0.23, 1, 0.32, 1) infinite;
                    content: '';
                }
                @keyframes Progress-active {
                    0% {
                        width: 0;
                        opacity: 0.1;
                    }
                    20% {
                        width: 0;
                        opacity: 0.8;
                    }
                    100% {
                        width: 100%;
                        opacity: 0;
                    }
                }
            }
        }

        > .xin-icon {
            ${_text}
            margin-top: 1px;
            margin-bottom: 1px;
        }

        > div.Progress-text {
            ${_text}
            text-align: right;
            color: rgba(0, 0, 0, 0.72);
        }
    }

    &.circle {
        width: 200px;
        height: 200px;

        .xin-icon,
        div.Progress-text {
            position: absolute;
            top: 0;
            left: 0;
            width: inherit;
            height: inherit;
            color: rgba(0, 0, 0, 0.72);
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 150%;
        }

        .xin-icon {
            font-size: 200%;
        }
    }
`;
interface ICircleProps {
    percent: number;
}

const _path = (percent: number) => {
    if (percent === 0) {
        return `stroke-dasharray: 0px, 295.31px; stroke-width: 0;`;
    } else if (percent >= 100) {
        return `stroke-dasharray: 295.31px, 295.31px;`;
    } else {
        return ` stroke-dasharray: ${(percent - 1) * 2.9531}px, 295.31px;`;
    }
};

export const StyledCircle = styled.svg<ICircleProps>`
    width: inherit;

    > path {
        stroke-dashoffset: 0px;
        d: path('M 50 50 m 0 -47 a 47 47 0 1 1 0 94 a 47 47 0 1 1 0 -94');
        stroke-linecap: round;
        stroke-width: 6;
        fill-opacity: 0;
        transition: stroke-dashoffset 0.3s ease 0s, stroke-dasharray 0.3s ease 0s, stroke 0.3s ease 0s, stroke-width 0.06s ease 0.3s;

        &.Progress-circle-trail {
            stroke: rgb(243, 243, 243);
            stroke-dasharray: 295.31px, 295.31px;
        }

        &.Progress-circle-path {
            stroke: rgb(16, 142, 233);
            ${props => _path(props.percent)}

            &.exception {
                stroke: #d0104c;
            }
            &.fill {
                stroke: #86c166;
            }
        }
    }
`;
