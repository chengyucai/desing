import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledProgress, StyledCircle } from './scss/style';
import Icon from '../Icon';
import * as SVG from '../Message/SVG';

export interface IProgressProps extends ICommonProps {
    onClick?: () => void;
    format?: (percent: number) => string;

    percent?: number;
    type?: 'line' | 'circle' | 'dashboard';
    status?: 'active' | 'exception';
}
const classnames = 'Progress';

const _text = (props: IProgressProps) => {
    const { percent, format, status } = props;

    if (percent >= 100) {
        return <Icon component={SVG.success} />;
    } else if (status === 'exception') {
        return <Icon component={SVG.error} />;
    } else {
        return <div className={cx(`${classnames}-text`)}>{format(percent)}</div>;
    }
};

const Module: React.FC<IProgressProps> = props => {
    const { onClick, percent, type, status } = props;
    return (
        <StyledProgress className={cx(classnames, 'ld-progress', props.className, type)} onClick={onClick} percent={percent}>
            {type === 'line' && (
                <div className={`${classnames}-inner`}>
                    <div className={cx(`${classnames}-bg`, status, { fill: percent >= 100 })} />
                </div>
            )}
            {type === 'circle' && (
                <StyledCircle className={`${classnames}-circle`} viewBox="0 0 100 100" percent={percent}>
                    <path className={`${classnames}-circle-trail`} />
                    <path className={cx(`${classnames}-circle-path`, status, { fill: percent >= 100 })} />
                </StyledCircle>
            )}
            {_text(props)}
        </StyledProgress>
    );
};

Module.defaultProps = {
    onClick: () => console.log('Progress'),
    format: percent => `${percent}%`,
    type: 'line',
    percent: 0,
};

export default Module;
