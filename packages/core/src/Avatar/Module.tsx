import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledAvatar, StyledNameIcon } from './scss/style';

export interface IAvatarProps extends ICommonProps {
    onCallback?: () => void;
    size?: number;
    shape?: 'circle' | 'square';
    color?: string;
    src?: string;
    alt?: string;
    title?: string;
}

const Module: React.FC<IAvatarProps> = props => {
    const { onCallback, size, shape, src, title, children, ...other } = props;
    return (
        <StyledAvatar className={cx('Avatar', 'ld-avatar', shape, props.className)} onClick={() => onCallback && onCallback()} size={size} url={src} {...other}>
            {children}
            {!children && !src && title && <StyledNameIcon size={size}>{title.match(/.{0,2}/)[0]}</StyledNameIcon>}
        </StyledAvatar>
    );
};

Module.defaultProps = {
    size: 48,
    color: 'darkgray',
    // alt: '載入失效',
};

export default Module;
