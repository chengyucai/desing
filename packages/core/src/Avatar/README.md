# `Avatar`

用作代表用戶或事物，支持圖片，圖標或字符展示。

## Example Usage

```js
var Avatar = require('Avatar');

<Avatar />
```

## `API`

Property    | Description               |Type              | Default  |
----------  |----------                 |------------      |----------|
| children  | 設置頭像的自定義           | ReactNode         | - |  
| shape     | 指定頭像的形狀             | Enum{ 'circle', 'square' } | `circle` |  
| size      | 設置頭像的大小             | number            | 48 |  
| src       | 圖片類頭像的資源地址        | string            | - |  
| alt       | 圖像無法顯示時的替代文本      | string          | - |  
| title     | 用來當滑鼠移經圖片時，顯示圖片的文字標示 | string          | - |  