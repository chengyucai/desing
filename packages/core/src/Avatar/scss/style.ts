import styled from 'styled-components';
import { IAvatarProps } from '../Module';

interface IAvatar_P extends IAvatarProps {
    url: string;
}

const _background = (props: IAvatar_P) => {
    return props.url
        ? `background-image: url(${props.url});
    background-repeat: no-repeat;
    background-size: contain;`
        : `background-color: ${props.color};`;
};

const _boxLayout = (size: number) => {
    return `display: flex;
    height: inherit;
    align-items: center;
    justify-content: center;
    color: azure;
    font-size: ${size}px;`;
};

export const StyledAvatar = styled.div<IAvatar_P>`
    width: ${props => `${props.size}px`};
    height: ${props => `${props.size}px`};
    border-radius: ${props => `${props.size}px`};

    ${props => _background(props)}

    overflow: hidden;
    cursor: pointer;
    background-position: center center;
    background-size: cover;


    &::after {
        content: '${props => props.alt}';
        background-color: darkgray;
        position: relative;
        ${props => _boxLayout(props.size / 3.4)}
        text-align: center;
        padding: 2px;
        box-sizing: border-box;
        z-index: -1;
    }

    &.square {
        border-radius: 4px;
    }
`;

export const StyledNameIcon = styled.div`
    ${(props: { size: number }) => _boxLayout(props.size / 2.3)};
    z-index: 1;
`;
