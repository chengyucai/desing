import styled from 'styled-components';

export const StyledBackTop = styled.div`
    position: fixed;
    width: fit-content;
    display: inline-block;

    bottom: 0;
    right: 0;
    margin: 30px;
    cursor: pointer;
    z-index: -2;
    transition: 0.4s;
    opacity: 0;
    &.show {
        opacity: 1;
        z-index: 98;
    }
`;

export const StyledDefaulted = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 48px;
    height: 48px;
    border-radius: 8px;
    background-color: #333e;
    color: #ffff;
    font-size: 200%;
`;
