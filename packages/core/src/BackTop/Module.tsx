import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledBackTop, StyledDefaulted } from './scss/style';

export interface IBackTopProps extends ICommonProps {
    onCallback?: () => void;
    limit?: number;
}

const Module: React.FC<IBackTopProps> = props => {
    const { onCallback, children, limit } = props;

    const [scrollY, setscrollY] = React.useState(0);

    const _Scroll = () => {
        setscrollY(window.scrollY);
    };

    React.useEffect(() => {
        document.addEventListener('scroll', _Scroll);
        return () => {
            document.removeEventListener('scroll', _Scroll);
        };
    }, []);

    const _Show = () => {
        return scrollY > limit;
    };

    const _onClick = () => {
        onCallback && onCallback();
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };

    return (
        <StyledBackTop className={cx('BackTop', 'ld-backtop', { show: _Show() }, props.className)} onClick={_onClick} data-testid={'ld-backtop'}>
            {children ? (
                children
            ) : (
                <StyledDefaulted className={cx('defaulted')}>
                    <span>&#8613;</span>
                </StyledDefaulted>
            )}
        </StyledBackTop>
    );
};

Module.defaultProps = {
    limit: 200,
};

export default Module;
