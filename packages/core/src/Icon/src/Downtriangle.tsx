import * as React from 'react';

const SvgDowntriangle = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 213.333 213.333" width="1em" height="1em" {...props} fill="currentColor">
        <path d="M0 53.333L106.667 160 213.333 53.333z" />
    </svg>
);

export default SvgDowntriangle;
