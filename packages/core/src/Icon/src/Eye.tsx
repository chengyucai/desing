import * as React from 'react';

const SvgEye = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 16 12" {...props} fill="currentColor">
        <path
            d="M8.123.63C4.543.63 1.486 2.857.248 6c1.238 3.143 4.295 5.37 7.875 5.37S14.76 9.143 15.999 6C14.76 2.857 11.706.63 8.123.63zm0 8.95A3.581 3.581 0 1111.703 6a3.581 3.581 0 01-3.58 3.58zm0-5.728A2.15 2.15 0 005.975 6a2.15 2.15 0 002.148 2.148A2.15 2.15 0 0010.271 6a2.15 2.15 0 00-2.148-2.148z"
            fill="#666"
            fillRule="nonzero"
        />
    </svg>
);

export default SvgEye;
