import * as React from 'react';

const SvgCheck = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M41.78 10.386a1.296 1.296 0 011.846 1.82l-25.578 25.93a1.296 1.296 0 01-1.906-.067L4.312 24.295a1.296 1.296 0 111.968-1.69l10.911 12.707 24.59-24.926z" />
    </svg>
);

export default SvgCheck;
