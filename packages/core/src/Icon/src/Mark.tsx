import * as React from 'react';

const SvgMark = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 16 16" {...props} fill="currentColor">
        <path
            d="M3.715 14a.754.754 0 01-.35-.087.66.66 0 01-.365-.58v-9.78C2.98 2.721 3.68 2.03 4.572 2h6.856c.891.029 1.592.721 1.572 1.553v9.78a.66.66 0 01-.358.574.759.759 0 01-.714 0l-4.05-2.14L4.073 13.9a.752.752 0 01-.357.1z"
            fill="#666"
            fillRule="nonzero"
        />
    </svg>
);

export default SvgMark;
