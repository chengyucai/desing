import * as React from 'react';

const SvgArrowDown = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M34.727 16.727l2.546 2.546-12 12.004a1.8 1.8 0 01-2.546 0l-12-12.004 2.546-2.546L24 27.457l10.727-10.73z" />
    </svg>
);

export default SvgArrowDown;
