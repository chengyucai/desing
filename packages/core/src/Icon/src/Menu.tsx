import * as React from 'react';

const SvgMenu = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M5.4 12h37.2a1.4 1.4 0 010 2.8H5.4a1.4 1.4 0 010-2.8zm0 10.6h37.2a1.4 1.4 0 010 2.8H5.4a1.4 1.4 0 010-2.8zm0 10.6h37.2a1.4 1.4 0 010 2.8H5.4a1.4 1.4 0 010-2.8z" />
    </svg>
);

export default SvgMenu;
