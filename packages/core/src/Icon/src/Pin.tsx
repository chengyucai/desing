import * as React from 'react';

const SvgPin = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 12 14" {...props} fill="currentColor">
        <path
            d="M6 0C2.686 0 0 2.601 0 5.81c0 1.48.572 2.831 1.513 3.857a.585.585 0 00.094.118l3.889 3.944a.59.59 0 00.182.167.615.615 0 00.767-.066.58.58 0 00.123-.172l3.818-3.873c.03-.029.057-.06.08-.094A5.687 5.687 0 0012 5.811C12 2.6 9.314 0 6 0zm-.003 8.192c-1.34 0-2.425-1.051-2.425-2.348s1.086-2.348 2.425-2.348c1.34 0 2.425 1.05 2.425 2.348 0 1.297-1.085 2.348-2.425 2.348z"
            fill="#666"
            fillRule="nonzero"
        />
    </svg>
);

export default SvgPin;
