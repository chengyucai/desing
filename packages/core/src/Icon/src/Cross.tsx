import * as React from 'react';

const SvgCross = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M12.439 34.061c.292.293.682.44 1.065.44.382 0 .765-.147 1.057-.44l8.693-8.69 8.685 8.69c.293.293.683.44 1.065.44a1.502 1.502 0 001.058-2.561l-8.686-8.69 8.686-8.689a1.502 1.502 0 00-2.123-2.122l-8.685 8.69-8.693-8.69a1.502 1.502 0 00-2.122 2.122l8.692 8.689-8.692 8.69a1.502 1.502 0 000 2.121z" />
    </svg>
);

export default SvgCross;
