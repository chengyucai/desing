import * as React from 'react';

const SvgArrowRight = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 32 32" width="1em" height="1em" {...props} fill="currentColor">
        <path d="M1 1l30 15L1 31l10-15L1 1z" strokeLinejoin="round" strokeLinecap="round" strokeWidth={2} />
    </svg>
);

export default SvgArrowRight;
