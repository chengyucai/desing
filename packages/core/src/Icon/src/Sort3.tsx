import * as React from 'react';

const SvgSort3 = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 384 384" width="1em" height="1em" {...props}>
        <path d="M0 277.333h128V320H0zM0 170.667h256v42.667H0zM0 64h384v42.667H0z" />
    </svg>
);

export default SvgSort3;
