import * as React from 'react';

const SvgXinMore = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 24 24" {...props}>
        <path d="M4 4h4v4H4zm0 6h4v4H4zm0 6h4v4H4zm6-12h4v4h-4zm0 6h4v4h-4zm0 6h4v4h-4zm6-12h4v4h-4zm0 6h4v4h-4zm0 6h4v4h-4z" fill="currentColor" fillRule="evenodd" />
    </svg>
);

export default SvgXinMore;
