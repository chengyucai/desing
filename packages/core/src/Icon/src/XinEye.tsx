import * as React from 'react';

const SvgXinEye = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}>
        <g fill="none" fillRule="evenodd">
            <path fill="#FFF1ED" d="M-885-87H935v399H-885z" />
            <path fill="#FFF" d="M-10-13h277V87H-10z" />
            <path
                d="M8.123 2.63C4.543 2.63 1.486 4.857.248 8c1.238 3.143 4.295 5.37 7.875 5.37S14.76 11.143 15.999 8c-1.239-3.143-4.293-5.37-7.876-5.37zm0 8.95c-1.976 0-3.58-1.604-3.58-3.58s1.604-3.58 3.58-3.58 3.58 1.604 3.58 3.58-1.604 3.58-3.58 3.58zm0-5.728c-1.185 0-2.148.963-2.148 2.148s.963 2.148 2.148 2.148S10.271 9.185 10.271 8s-.963-2.148-2.148-2.148z"
                fill="#666"
                fillRule="nonzero"
            />
        </g>
    </svg>
);

export default SvgXinEye;
