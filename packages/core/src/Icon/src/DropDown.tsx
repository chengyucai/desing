import * as React from 'react';

const SvgDropDown = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 384 384" width="1em" height="1em" {...props} fill="currentColor">
        <path d="M0 21.333l192 341.333L384 21.333H0zM72 64h240L192 277.333 72 64z" />
    </svg>
);

export default SvgDropDown;
