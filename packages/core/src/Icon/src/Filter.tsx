import * as React from 'react';

const SvgFilter = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M8.658 7.3l12.368 15.902A1.3 1.3 0 0121.3 24v15.4l5.4-4.05V24c0-.29.096-.57.274-.798L39.343 7.3H8.658zM18.7 24.446L4.974 6.798C4.31 5.944 4.918 4.7 6 4.7h36.001c1.082 0 1.69 1.244 1.026 2.098L29.3 24.446V36a1.3 1.3 0 01-.52 1.04l-8 6c-.857.643-2.08.031-2.08-1.04V24.446z" />
    </svg>
);

export default SvgFilter;
