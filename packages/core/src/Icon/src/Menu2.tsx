import * as React from 'react';

const SvgMenu2 = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 24 25" fill="currentColor" {...props}>
        <g fill="none" fillRule="evenodd">
            <path d="M0 2h24v24H0z" />
            <path fill="currentColor" d="M5 5.94h15v2H5zm0 12.829h15v2H5zM5 12h10v2H5z" />
        </g>
    </svg>
);

export default SvgMenu2;
