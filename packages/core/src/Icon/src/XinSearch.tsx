import * as React from 'react';

const SvgXinSearch = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 24 24" {...props}>
        <path
            d="M21.22 19.857l-3.42-3.403c2.861-3.47 2.662-8.613-.61-11.865-3.468-3.452-9.103-3.452-12.586 0-3.47 3.464-3.47 9.068 0 12.518 3.27 3.254 8.44 3.452 11.93.606l3.422 3.403c.075.066.194.066.258 0l1.008-1.002c.065-.063.065-.182 0-.257zm-5.273-3.986a7.155 7.155 0 01-10.078 0 7.062 7.062 0 010-10.023 7.155 7.155 0 0110.078 0 7.062 7.062 0 010 10.023z"
            fill="currentColor"
        />
    </svg>
);

export default SvgXinSearch;
