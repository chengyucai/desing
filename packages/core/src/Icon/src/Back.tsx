import * as React from 'react';

const SvgBack = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 48 48" {...props} fill="currentColor">
        <path d="M8.268 22.754H42.76c.69 0 1.24.56 1.24 1.25s-.55 1.25-1.24 1.25H8.268l9.18 9.18A1.25 1.25 0 0115.68 36.2L4.366 24.888a1.25 1.25 0 010-1.768L15.68 11.806a1.25 1.25 0 011.768 1.768l-9.18 9.18z" />
    </svg>
);

export default SvgBack;
