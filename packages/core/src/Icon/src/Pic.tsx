import * as React from 'react';

const SvgPic = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 96 96" {...props} fill="currentColor">
        <path d="M.5 3.5v89a3 3 0 003 3h89a3 3 0 003-3v-89a3 3 0 00-3-3h-89a3 3 0 00-3 3zm90.4 84.028h-86l22.346-40.6 15.017 26.499 21.672-34.76 26.859 48.86h.106z" fill="#666" fillRule="nonzero" />
    </svg>
);

export default SvgPic;
