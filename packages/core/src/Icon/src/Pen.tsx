import * as React from 'react';

const SvgWhitePen = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 14 14" {...props} fill="currentColor">
        <path
            d="M14 3.668a.7.7 0 00-.203-.497L10.829.203A.699.699 0 0010.332 0a.7.7 0 00-.497.203l-1.981 1.98L.203 9.834a.7.7 0 00-.203.497V13.3a.7.7 0 00.7.7h2.968a.704.704 0 00.532-.203l7.609-7.65 1.988-1.946a.833.833 0 00.154-.231.702.702 0 00.049-.301v-.001zM3.381 12.599H1.4v-1.98l6.951-6.951 1.981 1.98-6.951 6.951zm7.938-7.937L9.338 2.681l.994-.987 1.974 1.974-.987.994z"
            fill="currentColor"
            fillRule="nonzero"
        />
    </svg>
);

export default SvgWhitePen;
