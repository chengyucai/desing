import * as React from 'react';

const SvgArrowLeft = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 32 32" width="1em" height="1em" {...props} fill="currentColor">
        <path d="M31 31L1 16 31 1 21 16l10 15z" strokeLinejoin="round" strokeLinecap="round" strokeWidth={2} />
    </svg>
);

export default SvgArrowLeft;
