import * as React from 'react';

const SvgArrowLeft2 = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 30 30" {...props}>
        <path
            fill="currentColor"
            d="M6.776 15.26a.816.816 0 00.547-.203L22.805 1.185a.644.644 0 000-.982.843.843 0 00-1.096 0L6.227 14.075a.644.644 0 000 .982.82.82 0 00.55.204zM23.225 30a.824.824 0 00.548-.204.644.644 0 000-.982L7.323 14.075a.84.84 0 00-1.096 0 .644.644 0 000 .982l16.45 14.739c.151.135.35.204.548.204z"
        />
    </svg>
);

export default SvgArrowLeft2;
