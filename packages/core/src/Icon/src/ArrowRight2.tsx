import * as React from 'react';

const SvgArrowRight2 = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 30 30" {...props}>
        <path
            fill="currentColor"
            d="M23.224 15.26a.816.816 0 01-.547-.203L7.195 1.185a.644.644 0 010-.982.843.843 0 011.096 0l15.482 13.872a.644.644 0 010 .982.82.82 0 01-.55.204zM6.775 30a.824.824 0 01-.548-.204.644.644 0 010-.982l16.45-14.739a.84.84 0 011.096 0 .644.644 0 010 .982L7.323 29.796a.824.824 0 01-.548.204z"
        />
    </svg>
);

export default SvgArrowRight2;
