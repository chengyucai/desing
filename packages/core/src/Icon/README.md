# `Icon`

撰寫`README.md`時，請將模組的使用規範規則與步驟規範清楚，使可讀性提高。


`README.md`中包含定義與說明`Icon`組件所需的功能。它通常與**專案**一起產出，請用簡單的方式寫出易讀性高的文章，~~請統一且美觀的呈現~~。

**Note:** 討論專案中需要哪些功能，或是說明要修改的內容有哪些。

## 加入新 Icon

將 svg 檔案放置於，assets 資料夾中(如果沒有請自行新增)，於專案根目錄，執行如下指令。執行完畢後，會產生 tsx 檔案於 src 資料夾中，並自動刪除放置於 assets 中的 svg 檔案。

```bash
yarn build:icon
```

產生出來的檔案，如需要做顏色調整，需針對檔案作微調，增加 svg 屬性 fill="currentColor"，並將 svg 內中的 path 、g 有 fill 之屬性進行刪除(需視狀況)。

```js
<svg width="1em" height="1em" fill="currentColor">...</svg>
```

```js
var Icon = require('Icon');
```


