import * as React from 'react';
import _ from 'lodash';
import cx from 'classnames';
import { StyledContainerIcon } from './scss/style';

export interface IIconPropsProperties {
    type: string;
    component: React.ReactNode;
}

export type TIconProps =
    | (Pick<IIconPropsProperties, 'type'> & Partial<Pick<IIconPropsProperties, 'component'>>)
    | ((Pick<IIconPropsProperties, 'component'> & Partial<Pick<IIconPropsProperties, 'type'>>) & React.SVGProps<SVGSVGElement>);

const Module = (props: TIconProps) => {
    let Icon = null;
    const { color, fontSize, className, ...prop }: any = props;
    if (!props.component && props.type) {
        Icon = require(`./src/${_.upperFirst(_.camelCase(props.type))}`).default;
    } else {
        Icon = props.component;
    }

    return (
        <StyledContainerIcon className={cx('xin-icon', 'ld-icon', { [className]: className })} {...prop}>
            {props.component ? <Icon /> : <Icon type={props.type} color={color} fontSize={fontSize} />}
        </StyledContainerIcon>
    );
};

Module.defaultProps = {};

export default Module;
