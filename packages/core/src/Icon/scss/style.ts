import styled from 'styled-components';

export const StyledContainerIcon = styled.div<any>`
    display: inline-block;
    font-size: 26px;
    height: 1em;
`;
