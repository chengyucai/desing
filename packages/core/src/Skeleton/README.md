# `Skeleton`





## Example Usage

```js
import Skeleton from '@packages/core/src/Skeleton';


    ReactDOM.render(
        <div>
            <span>Facebook</span>
            <div style={{ display: 'flex' }} className={cusClass.custom}>
                <Module shape={'circle'} />
                <div className={cusClass.fbTitleDateWrap}>
                    <Module length="25%" shape={'text'} className={cusClass.textClass} />
                    <Module length="50%" shape={'text'} className={cusClass.textClass} />
                </div>
            </div>

            <Module shape={'square'} className={cusClass.fbContent} />
        </div>
        mountNode
    );



```



### API
|Property|Description|Type|Default|
|--------|-----------|----|--------|
|shape|骨架屏形狀|String||
|animation| 動畫產生| boolean| false|