import * as React from 'react';
import { default as cx } from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledSkeleton } from './scss/style';

export interface ISkeletonProps extends ICommonProps {
    shape?: 'square' | 'circle';
    length?: string;
    shineColor?: string;
    animation?: boolean;
    speed?: number;
    style?: React.CSSProperties;
}

const Module: React.FC<ISkeletonProps> = props => {
    const { shineColor, animation, style, length, speed } = props;

    return <StyledSkeleton style={style} length={length} shineColor={shineColor} speed={speed} className={cx('Skeleton', 'ld-skeleton', props.shape, props.className, { animation: animation })} />;
};

Module.defaultProps = {
    length: '100%',
    shineColor: 'rgb(216, 216, 216)',
    shape: 'square',
    speed: 1.5,
    animation: true,
};

export default Module;
