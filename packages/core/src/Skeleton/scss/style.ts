import styled from 'styled-components';

interface ISkeletonProps {
    length: string;
    shineColor: string;
    speed: number;
}

export const StyledSkeleton = styled.div<ISkeletonProps>`
    position: relative;
    height: ${props => props.length};
    width: ${props => props.length};
    background-color: ${props => props.shineColor};
    margin: 0;

    @keyframes shine {
        0%,
        100% {
            opacity: 1;
        }
        50% {
            opacity: 0.5;
        }
    }

    &.circle {
        border-radius: 50%;
    }

    &.square {
    }

    &.animation {
        animation: shine ${props => props.speed}s infinite ease-in-out;
    }
`;
