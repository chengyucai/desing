import * as React from 'react';
import cx from 'classnames';
import { StyledLightBoxCloseBtn, StyledLightBoxContentBox, StyledLightBoxWrapper } from './scss/style';
import Icon from '../Icon';
import Modal, { IModal } from '../Modal';

interface ILightBox extends Omit<IModal, 'onClose'> {
    closeBtn?: boolean; //叉叉按鈕
    ToggleLightBox: (isOpen: boolean) => void;
}

const Module: React.FC<ILightBox> = props => {
    return (
        <Modal {...props} className={cx('LightBox', 'ld-lightbox', props.className)} onClose={() => props.ToggleLightBox(false)}>
            <StyledLightBoxWrapper>
                {/* 關閉燈箱叉叉 */}
                {props.closeBtn && (
                    <StyledLightBoxCloseBtn type={'button'} onClick={() => props.ToggleLightBox(false)}>
                        {<Icon type={'cross'} />}
                    </StyledLightBoxCloseBtn>
                )}
                <StyledLightBoxContentBox>{props.children}</StyledLightBoxContentBox>
            </StyledLightBoxWrapper>
        </Modal>
    );
};

Module.defaultProps = {
    open: false,
    closeBtn: true,
    clickOutside: true,
};

export default Module;
