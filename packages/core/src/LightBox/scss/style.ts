import styled from 'styled-components';

export const StyledLightBoxWrapper = styled.div`
    padding: 35px 15px 15px;
    position: relative;
`;

export const StyledLightBoxContentBox = styled.div`
    width: 100%;
    height: auto;
    max-height: 80vh;
    overflow: auto;
`;

export const StyledLightBoxCloseBtn = styled.button`
    position: absolute;
    top: 10px;
    right: 10px;
    font-size: 20px;
    width: 30px;
    height: 30px;
    line-height: 30px;
    margin: 0;
    padding: 0;
    text-align: center;
    cursor: pointer;
    border: none;
    background: none;
    color: #888;
    transition: 0.3s;
    outline: none;
    &:hover {
        color: #000;
    }
`;
