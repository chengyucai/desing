const Color = {
    red: '#fff1f0',
    blue: '#e6f7ff',
    gold: '#fff8d2',
    magenta: '#fff0f6',
    orange: '#fff7e6',
    green: '#f6ffed',
    cadetblue: '#e6fffb',
    purple: '#f9f0ff',
};

export default Color;
