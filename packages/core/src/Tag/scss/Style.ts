import styled from 'styled-components';

export const TagSTY = styled.span`
    display: ${(props: { show: boolean }) => (props.show ? 'inline-flex' : 'none')};
    font-family: Microsoft JhengHei;
    box-sizing: border-box;
    margin-right: 8px;
    padding: 1px 7px;
    white-space: nowrap;
    background: #fafafa;
    border: 1px solid #d9d9d9;
    color: #222222;
    border-radius: 2px;
    font-size: ${({ theme }) => theme?.tag?.fontSize || '12px'};
    opacity: 1;
    cursor: default;
    &:hover {
        opacity: 0.85;
    }
    .xin-icon {
        display: flex;
        height: 100%;
        font-size: initial;
        align-items: center;
        svg {
            font-size: large;
        }
    }
`;

export const CrossSTY = styled.span`
    cursor: pointer;
    font-style: normal;
`;
