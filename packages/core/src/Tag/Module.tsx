import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { TagSTY, CrossSTY } from './scss/Style';
import Icon from '../Icon';
import ColorList from './color';

type Tcolor = keyof typeof ColorList;
export interface ITagProps extends ICommonProps {
    children: React.ReactNode;
    closable?: boolean;
    color?: string | Tcolor;
    onClick?: () => void;
    onClose?: () => void;
    visible?: boolean;
    closeIcon?: React.ReactNode;
}

const Module: React.FC<ITagProps> = props => {
    const { closable = false, visible = true, color, onClick, onClose, children, closeIcon } = props;

    const [show, setShow] = React.useState(true);

    const handleColor = React.useCallback(() => {
        if (!color) {
            return {};
        } else {
            if (ColorList.hasOwnProperty(color)) {
                return { background: ColorList[color as Tcolor], borderColor: color, color: color };
            } else {
                return { background: color, borderColor: 'transparent', color: '#FFF' };
            }
        }
    }, [color]);

    const handleClose = () => {
        setShow(false);
        onClose && onClose();
    };

    React.useEffect(() => {
        if (show !== visible) setShow(visible);
    }, [visible]);

    return (
        <TagSTY className={cx('Tag', 'ld-tag', props.className)} show={show} style={handleColor()} onClick={onClick}>
            {children}
            {closable &&
                (closeIcon ? (
                    <CrossSTY className="Cross" onClick={handleClose}>
                        {closeIcon}
                    </CrossSTY>
                ) : (
                    <Icon type="cross" onClick={handleClose} />
                ))}
        </TagSTY>
    );
};

export default Module;
