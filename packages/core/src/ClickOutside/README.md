
## `Example Usage`

```js
import ClickOutside from 'ClickOutside';

const { Trigger, Window } = ClickOutside;

<ClickOutside>
    <Trigger>
        <button>Hello</button>
    </Trigger>
    <Window>World</Window>
</ClickOutside>
```

# ClickOutside

Property    | Description     |Type              | Default  |
----------  |----------       |------------      |----------|
defaultOpen | 初始是否開啟狀態  |boolean           | false    |
ref         | 取得模組提供資源  |RefObject\<div>  | null     |
children    | 設置子元件       | Array< ReactNode>|          |


# Trigger
Property  | Description             |Type              | Default  |
----------|----------               |------------      |----------|
onClick   | 點擊\<Trigger>調用此函數 |function          | null     |
toggle    | 改變<Window>觸發狀態     |boolean           | false    |
children  | 設置子元件              | Arrat< ReactNode>|          |

# Window
Property  | Description             |Type              | Default  |
----------|----------               |------------      |----------|
closeBtn  | 設置默認關閉按鈕          |boolean           | false    |
onClick   | 點擊\<closeBtn>調用此函數|function          | null     |
children  | 設置子元件               | Arrat< ReactNode>|          |
