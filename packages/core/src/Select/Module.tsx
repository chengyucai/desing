import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import Icon from '../Icon';
import ClickOutside from '../ClickOutside';
import { SelectSTY, OptionSTY, MSelectSTY } from './scss/style';

type T_Data = { text: string; value: string | number };
const allData: T_Data = { text: '全部', value: 'all' };
export interface ISelectProps extends ICommonProps {
    data: T_Data[];
    defValue?: string | number;
    all?: boolean;
    allChange?: () => void;
    onChange?: (value: string | number) => void;
}
const { Window, Trigger } = ClickOutside;
const Module: React.FC<ISelectProps> = props => {
    const { data, defValue, all, allChange, onChange } = props;

    const [Data, setData] = React.useState<T_Data[]>([]);
    const [select, setSelect] = React.useState<T_Data>(undefined);
    const [open, setOpen] = React.useState(false);

    React.useEffect(() => {
        if (data?.length) {
            setData(all ? [allData].concat(data) : data);
            if (defValue !== null || defValue !== undefined) {
                setSelect(data.find(item => item.value === defValue));
            } else {
                setSelect(all ? allData : data[0]);
            }
        }
    }, [data, defValue]);

    const handleSelect = (item: { text: string; value: string | number }) => {
        setSelect(item);
        if (item.value === 'all' && allChange) {
            allChange();
        } else if (!!onChange) {
            onChange(item.value);
        }
    };

    return (
        <>
            <SelectSTY className={cx('Select', props.className)}>
                <ClickOutside onChange={state => setOpen(state)}>
                    <Trigger toggle className={cx({ open: open })}>
                        <span>{select?.text}</span>
                        <Icon type="arrow-down" />
                    </Trigger>
                    <Window close>
                        <OptionSTY>
                            {Data.map((item, index) => {
                                return (
                                    <div key={`Select_Option-${index}`} onClick={() => handleSelect(item)}>
                                        {item.text}
                                    </div>
                                );
                            })}
                        </OptionSTY>
                    </Window>
                </ClickOutside>
            </SelectSTY>
            <MSelectSTY
                value={select?.value}
                onChange={e => {
                    const { value } = e.target;
                    handleSelect({ text: Data.find(item => String(item.value) === value).text, value: value });
                }}
            >
                {Data.map((item, index) => {
                    return (
                        <option value={item.value} onChange={() => handleSelect(item)} key={`Select_Option-${index}`}>
                            {item.text}
                        </option>
                    );
                })}
            </MSelectSTY>
        </>
    );
};

Module.defaultProps = {};

export default Module;
