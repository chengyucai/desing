import styled from 'styled-components';

const _MediaMin = (theme: any) => {
    return theme?.screen?.laptop || '1200px';
};

// 主容器
export const BodySTY = styled.div`
    overflow: hidden;
    position: relative;
    width: 100%;
    user-select: none;
    z-index: 0;

    @media screen and (min-width: ${({ theme }) => _MediaMin(theme)}) {
    }
`;
// 外框容器
export const BoxSTY = styled.div<{ maxWidth?: number }>`
    position: relative;
    max-width: 100vw;
    width: 100%;
    margin: auto;

    &:hover {
        > .left {
            left: 0px !important;
        }
        > .right {
            right: 0px !important;
        }
    }

    @media screen and (min-width: ${({ maxWidth }) => maxWidth}px) {
        max-width: ${({ maxWidth }) => maxWidth}px;
        width: ${({ maxWidth }) => maxWidth}px;
        margin: 0 auto;

        .pre-pre,
        .next-next,
        .pre,
        .next {
            filter: brightness(0.64);
        }
        > .left,
        > .right {
            display: none;
        }
    }
`;
// 內框容器(ratio)
export const ModelSTY = styled.div<{ ratio: number }>`
    position: relative;
    padding: calc(100% / (${({ ratio }) => ratio} / 1) / 2) 0;
    width: 100%;
`;

export const ImgSTY = styled.div<{ url: string; transition: number }>`
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: url(${({ url }) => url});
    background-position: center center;
    background-repeat: no-repeat;
    background-size: 100% auto;
    background-color: aliceblue;

    transition-duration: ${({ transition }) => transition}ms;
    top: 0;
    left: 0%;
    z-index: 0;

    &.pre-pre,
    &.next-next {
        z-index: 1;
    }
    &.pre,
    &.next {
        cursor: pointer;
        z-index: 2;
    }

    &.protagonist {
        z-index: 4;
    }

    &.pre {
        left: -100%;
    }
    &.next {
        left: 100%;
    }
    &.pre-pre {
        left: -200%;
    }
    &.next-next {
        left: 200%;
    }

    &.null {
        display: none;
    }

    @media screen and (min-width: ${({ theme }) => _MediaMin(theme)}) {
    }
`;

export const BtnSTY = styled.div`
    position: absolute;
    display: flex;
    align-items: center;
    top: 0;
    height: 100%;
    z-index: 4;
    transition: 0.16s;
    cursor: pointer;
    user-select: none;

    &.left {
        left: -50px;
    }
    &.right {
        right: -50px;
    }

    > .xin-icon {
        display: flex;
        align-items: center;
        color: #222;
        background-color: #fff;
        padding: 20px 8px;
        border-radius: 0 4px 4px 0;

        &:hover {
            color: #fff;
            background-color: #555;
        }

        &[type='arrow-right-2'] {
            border-radius: 4px 0 0 4px;
        }
    }
    @media screen and (min-width: ${({ theme }) => _MediaMin(theme)}) {
    }
`;

export const SlickSTY = styled.div`
    position: absolute;
    display: grid;
    justify-content: center;
    bottom: 0;
    grid-auto-flow: column;
    width: 100%;
    z-index: 4;
    > div {
        cursor: pointer;
        padding: 8px 2px;
        &.active {
            > div {
                opacity: 1;
                width: 32px;
            }
        }

        > div {
            background-color: #fff;
            height: 4px;
            width: 20px;
            opacity: 0.57;
            transition: all 0.2s;
        }
    }
    @media screen and (min-width: ${({ theme }) => _MediaMin(theme)}) {
    }
`;

export default {};
