import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import Icon from '../Icon';
import { BodySTY, BoxSTY, ModelSTY, ImgSTY, BtnSTY, SlickSTY } from './scss/style';

interface ads {
    path: string;
    herf?: string;
}
interface I_Main extends ICommonProps, React.PropsWithChildren<any> {
    urls?: ads[];
    timer?: number; //自動換張時間(ms)
    ratio?: number;
    transition?: number;
    limitImg?: number;
    onSlick?: boolean;
    onChange?: (v: number) => void;
    // ...pop
    maxWidth?: number;
}

let CD = true;
let interval: any = null;

const Carousel: React.FC<I_Main> = props => {
    const { urls, timer, transition, ratio, className, onSlick, limitImg, onChange, children, ...pop } = props;
    const [Index, setIndex] = React.useState(0);

    const [deta, setDeta] = React.useState<ads[]>(null);
    const Next = React.useRef(null);

    // 輪播(核心)
    const _trn = (set: boolean, value = 1) => {
        if (!deta) return 0;
        const len = deta.length;
        const nextIndex = Index + (set ? value : -value);
        if (nextIndex >= len) {
            return nextIndex - len;
        } else if (nextIndex < 0) {
            return len + nextIndex;
        }
        return nextIndex;
    };
    // 自動輪播功能
    const _intervalInit = () => {
        if (timer) {
            interval = setInterval(() => {
                if (Next.current === null) {
                    clearInterval(interval);
                } else {
                    Next.current.click();
                }
            }, timer);
        }
    };
    // 圖片初始化
    React.useEffect(() => {
        if (urls && !deta) {
            if (urls.length >= limitImg) {
                setDeta(urls);
            } else {
                setDeta(new Array(limitImg).fill(null).map((_, index) => urls[index % urls.length]));
            }
        }
    }, [urls]);
    // 計時器初始化
    React.useEffect(() => {
        if (timer) {
            _intervalInit();
            return () => clearInterval(interval);
        } else {
            return () => console.log('Bye');
        }
    }, []);
    // 冷卻時間
    React.useEffect(() => {
        onChange && onChange(Index);
        CD = false;
        setTimeout(() => (CD = true), transition);
    }, [Index]);

    if (!deta) return null;
    return (
        <React.Fragment>
            <BodySTY onMouseLeave={() => _intervalInit()} onMouseOver={() => clearInterval(interval)}>
                <BoxSTY className={cx('Carousel', { [className || '']: className })} {...pop}>
                    <ModelSTY ratio={ratio}>
                        {children}
                        {deta?.map((item, index) => {
                            let name = '';
                            if (index === Index) {
                                name = 'protagonist';
                            } else if (index === _trn(true)) {
                                name = 'next';
                            } else if (index === _trn(false)) {
                                name = 'pre';
                            } else if (index === _trn(false, 2)) {
                                name = 'pre-pre';
                            } else if (index === _trn(true, 2)) {
                                name = 'next-next';
                            } else {
                                name = 'null';
                            }
                            return (
                                <ImgSTY
                                    url={item.path}
                                    transition={transition}
                                    className={name}
                                    onClick={() => {
                                        index === Index ? item.herf && window.open(item.herf, '_blank') : setIndex(index);
                                    }}
                                    key={`Carousel_Img-${index}`}
                                />
                            );
                        })}
                    </ModelSTY>
                    <BtnSTY className="left" onClick={() => CD && setIndex(_trn(false))}>
                        <Icon type={'arrow-left-2'} />
                    </BtnSTY>
                    <BtnSTY className="right" onClick={() => CD && setIndex(_trn(true))} ref={Next}>
                        <Icon type={'arrow-right-2'} />
                    </BtnSTY>
                    {!onSlick && !children && (
                        <SlickSTY className="Slick">
                            {deta?.map((_, index) => (
                                <div key={`SlickSTY-${index}`} className={cx({ active: index === Index })} onClick={() => setIndex(index)}>
                                    <div />
                                </div>
                            ))}
                        </SlickSTY>
                    )}
                </BoxSTY>
            </BodySTY>
        </React.Fragment>
    );
};

Carousel.defaultProps = {
    limitImg: 5,
    maxWidth: 800,
    ratio: 4 / 1,
    transition: 400,
};

export default Carousel;
