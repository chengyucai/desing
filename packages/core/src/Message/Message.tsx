import * as React from 'react';
import cx from 'classnames';
import Notice from './Notice';
import { StyledMessage } from './scss/style';

export interface IMessageSettings {
    isRepeat?: boolean;
    speed?: number;
}
interface IMessageProps extends IMessageSettings {
    onCallback?: (data: any) => void;
    text?: string;
    itemKey?: string;
    status?: 'success' | 'error' | 'info' | 'warning' | 'loading';
}

const Module: React.FC<IMessageProps> = props => {
    const { onCallback, text, status, isRepeat, speed, itemKey } = props;
    const [List, setList] = React.useState(new Array(0));
    const ListRef = React.useRef(List);
    ListRef.current = List;

    React.useEffect(() => {
        // 沒有text
        if (!text) return;
        if (!isRepeat && List.some(item => item.text === text)) return;

        const curKey = itemKey || Math.random().toString();

        setList(pre => {
            const newData = {
                text: text,
                status: status,
                KEY: curKey,
            };

            // 確認是否有自訂Key與重複
            const pref = itemKey && pre.some(item => item.KEY === curKey);
            // 上述條件:
            // 成立 => 找出重複位置後覆蓋新資料
            // 不成立 => 新資料加入最後一筆
            return pref ? pre.map(item => (item.KEY === curKey ? newData : item)) : [...pre, newData];
        });

        speed &&
            setTimeout(() => {
                setList(ListRef.current.filter(item => item.KEY !== curKey));
            }, speed);
    }, [props]);

    return (
        <StyledMessage className={cx('Message', 'ld-msg')} onClick={() => onCallback && onCallback(text)}>
            {List.map(item => {
                const { KEY, ...prop } = item;
                return <Notice {...prop} key={`Notice_${KEY}`} />;
            })}
        </StyledMessage>
    );
};

export default Module;
