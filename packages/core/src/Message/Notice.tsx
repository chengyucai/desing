import * as React from 'react';
import cx from 'classnames';
import { StyledNotice, StyledLoading } from './scss/style';
import Icon from '../Icon';
import * as SVG from './SVG';

interface INoticeProps {
    text: string;
    status?: 'success' | 'error' | 'info' | 'warning' | 'loading';
}

const Module: React.FC<INoticeProps> = props => {
    const { text, status } = props;

    return (
        <StyledNotice className={cx('Notice', 'ld-msg-notice')}>
            {status && (status !== 'loading' ? <Icon component={SVG[status]} style={{ fontSize: '100%' }} /> : <StyledLoading />)}
            <div className={'Icon_Notice_test'}>{text}</div>
        </StyledNotice>
    );
};

/**
 * Props default value write here
 */

export default React.memo(Module);
