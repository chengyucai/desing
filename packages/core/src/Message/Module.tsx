import ReactDOM from 'react-dom';
import * as React from 'react';

import Module, { IMessageSettings as IMS } from './Message';
import { merge } from 'lodash';

export type IMessageSettings = IMS;

const config: IMessageSettings = {
    speed: 1600,
    isRepeat: false,
};

interface renderItem extends IMessageSettings {
    status?: 'success' | 'error' | 'info' | 'warning' | 'loading';
    itemKey?: string;
}

type setFunction = (text: string, config?: IMessageSettings) => void;
interface Props {
    _info: setFunction;
    _error: setFunction;
    _success: setFunction;
    _warning: setFunction;
    _open: setFunction;
    _config: (config: IMessageSettings) => void;
    _loading: (method: () => any, text?: string, config?: IMessageSettings) => any;
}

class message implements Props {
    private config: IMessageSettings = config;
    private rootEl: any;

    constructor() {
        typeof document !== 'undefined' && this.addDOM();
    }

    _info(text: string, config?: IMessageSettings) {
        this.render(text, { ...config, status: 'info' });
    }

    _error(text: string, config?: IMessageSettings) {
        this.render(text, { ...config, status: 'error' });
    }

    _success(text: string, config?: IMessageSettings) {
        this.render(text, { ...config, status: 'success' });
    }

    _warning(text: string, config?: IMessageSettings) {
        this.render(text, { ...config, status: 'warning' });
    }

    _open(text: string, config?: IMessageSettings) {
        this.render(text, { ...config });
    }

    _config(config: IMessageSettings) {
        this.config = merge(this.config, config);
    }

    async _loading(method: () => any, text?: string, config?: IMessageSettings) {
        const KEY = new Date().getTime().toString();

        await this.render(text || 'Working...', { speed: 0, status: 'loading', itemKey: KEY });

        try {
            const result = await method();
            await this.render('OK', { status: 'success', itemKey: KEY, ...config });
            return result;
        } catch (error) {
            await this.render(error.toString().match(/:(.*)/)[1], { status: 'error', itemKey: KEY, ...config });
        }
    }

    private render(text: string, item: renderItem) {
        !this.rootEl && this.addDOM();
        const Setting = { ...this.config };
        ReactDOM.render(<Module {...merge(Setting, item)} text={text} />, this.rootEl);
    }

    private addDOM() {
        this.rootEl = document.createElement('div');
        document.body.appendChild(this.rootEl);
    }
}
const Message = new message();

export default Message;
