import styled from 'styled-components';

export const StyledMessage = styled.div`
    position: fixed;
    display: flex;
    width: 100%;
    flex-flow: column;
    align-items: center;
    top: 20px;
    left: 0;
    height: 0;
    z-index: 9999;
`;

export const StyledNotice = styled.div`
    display: flex;
    align-items: center;
    padding: 15px 16px;
    margin: 10px 0;
    background: #fff;
    border-radius: 4px;
    -webkit-box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
    box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
    pointer-events: all;
    animation: show 0.4s;
    animation-fill-mode: forwards;
    z-index: 9999;

    .Icon_Notice_test {
        margin-left: 4px;
    }
    font-size: ${({ theme }) => theme?.Message?.size || '20px'};

    .Icon_Notice_test,
    > .xin-icon {
        float: left;
        display: flex;
        align-items: center;
        height: 100%;
    }

    @keyframes show {
        from {
            opacity: 0;
            transform: translateY(-16px);
        }
        to {
            opacity: 1;
        }
    }
`;

export const StyledLoading = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${({ theme }) => theme?.Message?.size || '20px'};
    height: ${({ theme }) => theme?.Message?.size || '20px'};
    float: left;
    &::after {
        content: '';
        width: 100%;
        height: 100%;
        background-color: deepskyblue;
        border-radius: 100%;
        opacity: 1;
        animation: Lion_loading 1.2s ease-out infinite;

        @keyframes Lion_loading {
            0% {
                transform: scale(0.1);
            }
            50% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                transform: scale(1);
            }
        }
    }
`;
