import styled from 'styled-components';
import { Props } from '../Module';
const _lineStyle = () => {
    return `
    height: 1px;
    background: #d8d8d8;`;
};
const _verticalStyle = () => {
    return `
    writing-mode: vertical-lr;
    text-orientation: mixed;
    `;
};

export const StyledDivider = styled.div`
    color: rgba(0, 0, 0, 0.75);
    font-size: ${(props: { font: number }) => props.font.toString()}px;

    &.vertical {
        display: inline-block;
        height: calc(100% - 20px);
        width: ${(props: Props) => (props.word ? 1 : 0)}px;
        margin: 0 ${(props: { font: number }) => (props.font / 2).toString()}px;
        ${_verticalStyle()}
    }

    &:not(.vertical) {
        margin: ${(props: { font: number }) => (props.font * 1.5).toString()}px;
    }

    &.horizontal {
        margin: ${(props: { font: number }) => `${props.font * 2.25}px ${props.font * 1.5}px`};
        display: block;
        ${_lineStyle()}
    }
`;

export const StyledText = styled.span`
    display: flex;
    align-items: center;
    font-weight: bold;
    white-space: nowrap;

    &::before {
        ${_lineStyle()}
        width: 100%;
        margin-right: ${(props: { font: number }) => (props.font / 1.5).toString()}px;
        content: '';
    }

    &::after {
        ${_lineStyle()}
        width: 100%;
        margin-left: ${(props: { font: number }) => (props.font / 1.5).toString()}px;
        content: '';
    }

    &.left {
        &::before {
            width: 5%;
        }
    }

    &.right {
        &::after {
            width: 5%;
        }
    }
`;

export const StyledTextV = styled.span`
    display: flex;
    align-items: center;
    font-weight: bold;
    white-space: nowrap;

    &::before {
        ${_verticalStyle()}
        width: ${(props: { font: number }) => props.font || 0}px;
        height: 100%;
        margin-top: ${(props: { font: number }) => (props.font / 1.5).toString()}px;
        content: '';
        background: #d8d8d8;
    }

    &::after {
        ${_verticalStyle()}
        width: ${(props: { font: number }) => props.font || 0}px;
        height: 100%;
        margin-bottom: ${(props: { font: number }) => (props.font / 1.5).toString()}px;
        content: '';
        background: #d8d8d8;
    }
    &.bottom {
        &::after {
            height: 5%;
        }
    }
    &.top {
        &::before {
            height: 5%;
        }
    }
`;
