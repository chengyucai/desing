import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledDivider, StyledText, StyledTextV } from './scss/style';

export interface Props extends ICommonProps {
    word?: string;
    font?: number;
    vertical?: boolean;
    orientation?: 'bottom' | 'top' | 'left' | 'right' | 'default';
}

const Module: React.FC<Props> = props => {
    const { word, font, orientation, vertical } = props;

    return (
        <StyledDivider className={cx('Divider', 'ld-divider', props.className, { horizontal: !word && !vertical }, { vertical: vertical })} font={font}>
            {!vertical && word ? (
                <StyledText className={cx('text', orientation)} font={font}>
                    {word}
                </StyledText>
            ) : (
                <StyledTextV className={cx('text', orientation)} font={font}>
                    {word}
                </StyledTextV>
            )}
        </StyledDivider>
    );
};

Module.defaultProps = {
    font: 16,
};

export default Module;
