import * as React from 'react';
import * as ReactDOM from 'react-dom';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledModalWrapper, StyledModalContent } from './scss/style';

export interface IModal extends ICommonProps {
    open: boolean;
    clickOutside?: boolean;
    onClose: () => void;
    root?: Element;
    width?: number;
    style?: React.CSSProperties;
}

const Modal: React.FC<IModal> = props => {
    const refDocRoot = React.useRef<Element>(null);
    const refComp = React.useRef<HTMLDivElement>(null);

    // server-side 時沒有 document 可使用，這時用 portal 會壞掉，因此用 state 判別是否進 client side
    const [isMount, setIsMount] = React.useState<boolean>(false);

    React.useEffect(() => {
        setIsMount(true);

        refDocRoot.current = props.root || document.body;
        refComp.current = document.createElement('div');
        refComp.current.dataset.testid = 'ld-modal';
        refDocRoot.current.appendChild(refComp.current);
        return () => {
            refDocRoot.current.removeChild(refComp.current);
        };
    }, []);

    React.useEffect(() => {
        /* 限制body捲動 */
        document.body.style.overflow = props.open ? 'hidden' : '';
    }, [props.open]);

    return isMount
        ? ReactDOM.createPortal(
              props.open ? (
                  <StyledModalWrapper className={cx('ld-modal', props.className)} onClick={() => props.clickOutside && props.onClose()}>
                      <StyledModalContent
                          className={'modal-content'}
                          width={props.width}
                          onClick={e => {
                              e.stopPropagation();
                          }}
                          style={props.style || null}
                      >
                          {props.children}
                      </StyledModalContent>
                  </StyledModalWrapper>
              ) : null,
              // 第二個參數指定元件要掛載到這個 DOM element
              refComp.current,
          )
        : null;
};

Modal.defaultProps = {
    className: '',
    open: false,
    clickOutside: true,
    width: 600,
};

export default Modal;
