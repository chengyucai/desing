import styled from 'styled-components';

export const StyledModalWrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);
    transition: 0.3s;
    z-index: 999;
`;

export const StyledModalContent = styled.div<{ width: number }>`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: ${props => props.width}px;
    max-width: 95%;
    height: auto;
    background: #fff;
    border-radius: 5px;
    box-sizing: border-box;
    z-index: 999;
`;
