import * as React from 'react';
import cx from 'classnames';
import { ICommonProps } from '../../utils';
import { StyledDrawer, StyledBox } from './scss/style';
import Icon from '../Icon';

export interface IDrawerProps extends ICommonProps {
    onClose?: () => void;
    visible?: boolean;
    closable?: boolean;
    placement?: 'top' | 'right' | 'bottom' | 'left';
    title?: string;
}

const classnames = 'Drawer';
const Module: React.FC<IDrawerProps> = props => {
    const { onClose, visible, placement, title, children, closable } = props;

    React.useEffect(() => {
        /* 限制body捲動 */
        document.body.style.overflow = visible ? 'hidden' : '';
    }, [visible]);

    return (
        <React.Fragment>
            <StyledDrawer className={cx(classnames, 'ld-drawer', props.className, { show: visible })} onClick={onClose} />
            <StyledBox className={cx(`${classnames}_box`, placement, { show: visible })}>
                {title && <div className={`${classnames}_header`}>{title}</div>}
                {children}
                {closable && <Icon type={'cross'} className={`${classnames}_close`} onClick={onClose} />}
            </StyledBox>
        </React.Fragment>
    );
};

Module.defaultProps = {
    onClose: () => console.log('close'),
    placement: 'right',
};

export default Module;
