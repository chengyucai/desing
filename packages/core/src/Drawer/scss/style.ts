import styled from 'styled-components';

export const StyledDrawer = styled.div`
    position: fixed;
    background-color: #000;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    z-index: -1;
    opacity: 0;
    transition: 0.3s;

    &.show {
        opacity: 0.57;
        z-index: 997;
    }
`;

export const StyledBox = styled.div`
    position: fixed;
    background-color: #fff;
    width: fit-content;
    z-index: 998;
    transition: 0.3s;
    opacity: 0;

    &.left {
        top: 0;
        left: 0;
        height: 100%;
        transform: translateX(-100%);
    }

    &.right {
        top: 0;
        right: 0;
        height: 100%;
        transform: translateX(100%);
    }

    &.top {
        top: 0;
        left: 0;
        width: 100%;
        transform: translateY(-100%);
    }

    &.bottom {
        bottom: 0;
        left: 0;
        width: 100%;
        transform: translateY(100%);
    }

    &.show {
        transform: translateX(0%);
        transform: translateY(0%);
        opacity: 1;
    }

    > .Drawer_close {
        position: absolute;
        top: 0;
        right: 0;
        padding: 8px;
        cursor: pointer;
    }
    > .Drawer_header {
        position: relative;
        padding: 12px 64px 12px 16px;
        color: rgba(0, 0, 0, 0.65);
        background: #fff;
        border-bottom: 1px solid #e8e8e8;
        border-radius: 4px 4px 0 0;
    }
`;
