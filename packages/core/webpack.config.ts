const path = require('path');
const merge = require('webpack-merge');
const root = path.resolve(__dirname, '../../');
const common = require(path.resolve(root, 'config/webpack.prod'));
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


const fs = require('fs');

require('../../config/utils/checkDefaultSafty')();

const entry: { [k: string]: any } = {};
// chech all component in index.ts
fs.readdirSync('./src').forEach((element: string) => {
    if (element !== '__mocks__') {
        const filename = `./src/${element}/index.ts`;
        entry[`${element}`] = filename;
    }
});
// build ssr version for publish
const config = merge(common, {
    target: 'node',
    entry: {
        index: './index.ts',
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, '/lib'),
        libraryTarget: 'umd',
    },
    module: {
        rules: [
            // {
            //     test: /\.svg$/,
            //     loader: [
            //         {
            //             loader: 'file-loader',
            //             options: {
            //                 name: 'assets/[name].[ext]',
            //             },
            //         },
            //         'svgo-loader',
            //     ],
            //     include: env.svg_loader_path,
            // },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
        // new BundleAnalyzerPlugin({ reportFilename: 'all.html', analyzerMode: 'static' }),
    ],
    externals: {
        'styled-components': 'styled-components',
    },
});

// build none target version for Hugo site
// const hugoConfig = merge(common, {
//     entry: {
//         index: './index.ts',
//     },
//     output: {
//         filename: '[name].js',
//         path: path.join(root, '/site/docs/v1/static/build'), // hugo 編譯後存放路徑
//         libraryTarget: 'umd',
//     },
//     plugins: [
//         new MiniCssExtractPlugin({
//             filename: '[name].css',
//             chunkFilename: '[id].css',
//         }),
//         new HtmlWebpackPlugin({
//             filename: path.resolve(root, 'site/docs/v1/layouts/partials/docs/inject/head.html'),
//             template: path.resolve(root, 'config/template/head.template.html'),
//             inject: false,
//             templateParameters: {
//                 path: '/build/index',
//             },
//         }),
//     ],
// });
// build for each component
// const cssConfig = merge(common, {
//     target: 'node',
//     entry: entry,
//     output: {
//         filename: '[name]/index.js',
//         path: path.join(__dirname, '/lib/src'),
//         libraryTarget: 'umd',
//     },
//     module: {
//         rules: [
//             {
//                 test: /\.svg$/,
//                 loader: [
//                     {
//                         loader: 'file-loader',
//                         options: {
//                             name: 'assets/[name].[ext]',
//                         },
//                     },
//                     {
//                         loader: 'svgo-loader',
//                         options: {
//                             plugins: [{ removeTitle: true }, { convertColors: { shorthex: false } }, { convertPathData: true }, { cleanupAttrs: true }],
//                         },
//                     },
//                 ],
//                 include: env.svg_loader_path,
//             },
//         ],
//     },
//     plugins: [
//         new MiniCssExtractPlugin({
//             filename: '[name]/index.css',
//             chunkFilename: '[id].css',
//         }),
//         new BundleAnalyzerPlugin({ reportFilename: 'each.html', analyzerMode: 'static' }),
//     ],
// });

// module.exports = [config, cssConfig, hugoConfig];
module.exports = [config];

export {};
