
# Xin-Design

React UI library.
欣傳媒版專用元件、色系放置處

## Demo

See the [website](http://10.35.2.26/).

## Install

```js
npm install @kade-design/tool
```

```js
yarn add @kade-design/tool
```

## Usage

```js
import { Button } from '@kade-design/tool';

const App = () => (
    <>
        <Button type="primary">primary</Button>
    <>
)

```

樣式需手動引入

```js
import '@kade-design/tool/lib/index.css';
```

或是使用 babel-plugin-import

.babelrc.js
```js
function firstUpperCase(str) {
    return str.toLowerCase().replace(/( |^)[a-z]/g, L => L.toUpperCase());
}

const presets = ['next/babel'];
const plugins = [
    [
        'import',
        {
            libraryName: '@kade-design/tool',
            libraryDirectory: 'lib/src',
            camel2UnderlineComponentName: false,
            camel2DashComponentName: false,
            style: function(name) {
                return `${firstUpperCase(name)}/index.css`;
            },
        },
        '@kade-design/tool',
    ],
    [
        'import',
        {
            libraryName: '@xin-design/icons',
            libraryDirectory: 'lib/src',
            camel2UnderlineComponentName: false,
            camel2DashComponentName: false,
            style: function(name) {
                return `@xin-design/icons/lib/index.css`;
            },
            customName: name => {
                if (name === 'createSvg') return `${name}.js`;
                return `@xin-design/icons/lib/src/${name.slice(0, name.length - 4)}.js`;
            },
        },
        '@xin-design/icons',
    ],
    './babelPlugins.js',
];

module.exports = { presets, plugins };
```

## Change Log
[CHANGELOG.md](https://devops.liontravel.com/LionF2E/XinMedia/_git/XinMedia-XinDesign?path=%2Fpackages%2Fcomponents%2FCHANGELOG.md&version=GBdevelop&fullScreen=true)