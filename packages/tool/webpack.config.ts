const path = require('path');
const merge = require('webpack-merge');
const root = path.resolve(__dirname, '../../');
const common = require(path.resolve(root, 'config/webpack.prod'));
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const fs = require('fs');

require('../../config/utils/checkDefaultSafty')('tool');

const entry: { [k: string]: any } = {};
// chech all component in index.ts
fs.readdirSync('./src').forEach((element: string) => {
    if (element !== '__mocks__') {
        const filename = `./src/${element}/index.ts`;
        entry[`${element}`] = filename;
    }
});
// build ssr version for publish
const config = merge(common, {
    target: 'node',
    entry: {
        index: './index.ts',
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, '/lib'),
        libraryTarget: 'umd',
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
        // new BundleAnalyzerPlugin({ reportFilename: 'all.html', analyzerMode: 'static' }),
    ],
    externals: {
        'styled-components': 'styled-components',
    },
});

module.exports = [config];

export {};
