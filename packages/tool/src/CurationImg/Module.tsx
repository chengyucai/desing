import * as React from 'react';
import _ from 'lodash';
import { BodySTY, TestSTY } from './scss/style';

export interface T_CurationImg {
    img?: React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;
    title?: string;
    sub?: string;
    titleStyle?: React.CSSProperties;
    subStyle?: React.CSSProperties;
    contentEditable?: boolean;
    onChange?: (v: T_CurationImg) => void;
}

const Module: React.FC<T_CurationImg> = props => {
    const { img, title, titleStyle, sub, subStyle, contentEditable, onChange } = props;

    const tRef = React.useRef<HTMLDivElement>(null);
    const sRef = React.useRef<HTMLDivElement>(null);

    React.useEffect(() => {
        tRef.current.innerText = title;
        sRef.current.innerText = sub;
    }, []);

    return (
        <BodySTY className="CurationImg" onBlur={() => onChange && onChange({ img: img, title: tRef.current.innerText, sub: sRef.current.innerText })}>
            <img {...img} onDragStart={e => e.preventDefault()} />
            <TestSTY>
                {!!title && <div ref={tRef} className="title" style={titleStyle} contentEditable={contentEditable} suppressContentEditableWarning={true} />}
                {!!sub && <div ref={sRef} className="sub" style={subStyle} contentEditable={contentEditable} suppressContentEditableWarning={true} />}
            </TestSTY>
        </BodySTY>
    );
};

// onInput={e => console.log(e.currentTarget.innerText)}
// onBlur={e => console.log('onBlur', tRef.current.innerText)}
Module.defaultProps = {
    img: { src: 'https://fakeimg.pl/400x100/' },
    title: '大標題',
    sub: '小標題',
};

export default Module;
