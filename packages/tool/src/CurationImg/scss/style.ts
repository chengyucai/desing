import styled from 'styled-components';
import { _DeviceWidth } from '../../theme';

export const BodySTY = styled.a`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;

    > img {
        width: 100%;
    }

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const TestSTY = styled.div`
    position: absolute;
    display: grid;
    text-align: center;
    div[contenteditable='true']:focus {
        outline: none;
        border: 1px dotted white;
        background-color: #0002;
    }

    > div.title {
        font-size: 20px;
        font-weight: bold;
    }
    > div.sub {
        font-size: 16px;
    }
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export default {};
