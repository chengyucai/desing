export const size = {
    mobile: '425px',
    tablet: '980px',
    laptop: '1280px',
    max: '1400px',
};

export const _DeviceWidth = (theme: any) => {
    return {
        mobile: theme?.screen?.mobile || size.mobile,
        tablet: theme?.screen?.tablet || size.tablet,
        laptop: theme?.screen?.laptop || size.laptop,
        max: theme?.screen?.max || size.max,
    };
};

export default {};
