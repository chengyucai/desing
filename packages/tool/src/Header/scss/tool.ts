import styled from 'styled-components';
import { _DeviceWidth } from '../../theme';

export const ToolSTY = styled.div`
    grid-area: tool;
    width: fit-content;
    margin-left: auto;
    user-select: none;

    .none {
        display: none;
    }

    .login {
        font-size: 16px;
        color: ${({ theme }) => theme?.XinMediaDesign?.Header?.ButtomColor};
        align-content: center;
    }

    .inputWrap {
        width: 100%;
        input {
            width: 100%;
        }
        .xin-icon {
            position: absolute;
            right: 10px;
            top: 1px;
            color: #bebebe;
        }
    }
`;

export const BodySTY = styled.div`
    display: grid;
    grid-auto-flow: column;
    .ClickOutside {
        .Trigger {
            display: flex;
            align-items: center;
            height: 44px;
            padding: 8px;
            cursor: pointer;
            .xin-icon {
                color: ${({ theme }) => theme?.XinMediaDesign?.Header?.ButtomColor};
                display: flex;
            }
        }
        .Window {
            position: fixed;
            width: 100%;
            top: 0;
            border-width: 0;
            padding: 0;
            background-color: #f6f6f6;
            z-index: 99;
            transition: all 0.3s;
            opacity: 1;
            transform: translateY(0%);

            &:not(.show) {
                display: block;
                top: 0;
                opacity: 0;
                transform: translateY(calc(-100% - 50px));
            }

            .closeBtn {
                position: relative;
                height: auto;
                display: flex;
                justify-content: flex-end;
                background-color: #fff;
                font-size: 32px;
                svg {
                    margin: 4px;
                }
            }
            &.paddingRight {
                padding-right: 0;
            }
            @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
                width: 250px;
                right: 0;
                top: auto;
                left: auto;
                border-radius: 3px;
                border-width: 1px;
                border-top-width: 0;
                border-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                &:not(.show),
                .closeBtn {
                    display: none;
                }
            }
            @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).max}) {
                right: calc((100% - 1400px) / 2);
            }
        }

        @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
            &.hover:hover .Window {
                display: block;
                top: auto;
                opacity: 1;
                transition: 0s;
                transform: none;
            }
            .Trigger {
                height: 56px;
                .Avatar {
                    height: 40px;
                    width: 40px;
                }
            }
        }
    }
`;

export const BoxSTY = styled.div`
    > div {
        color: ${({ theme }) => theme?.XinMediaDesign?.Header?.Link};
        font-size: 14px;
        padding: 8px;
    }
    .search-input .inputWrap {
        height: 32px;
        > input {
            flex: 1 1 auto;
            margin-right: 5px;
        }
        .search-btn {
            height: 100%;
            line-height: 32px;
            border: ${({ theme }) => `1px solid ${theme?.XinMediaDesign?.Header?.PrimaryColor}`};
            border-radius: 4px;
            background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            color: #fff;
            cursor: pointer;
            flex: 1 0 40px;
            text-align: center;
        }
    }
    .hotTag {
        margin-top: 4px;
        padding: 4px;
    }
    .serviceMenu {
        margin: 8px;
        > div {
            cursor: pointer;
            padding: 4px;
            margin: 0 2px;
        }
    }
    .to-more {
        margin: 30px 0;
        @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
            margin: 0;
        }
    }
    &.memberBox {
        .topWrap {
            > div {
                margin: 8px 0;
            }
            .Avatar {
                margin: 0 auto;
            }
            .nickName {
                word-wrap: break-word;
                font-size: 18px;
            }
            .idWrap {
                color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                .Divider {
                    height: auto;
                    .text::before,
                    .text::after {
                        width: 1px;
                        margin: 0;
                        background: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                    }
                }
            }
        }
        .travelPWrap {
            align-items: baseline;
            .point {
                font-size: 22px;
                margin: 0 10px;
                color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            }
        }
        .memberMenu {
            > div {
                align-items: center;
                color: #222;
                border: solid 1px transparent;
                cursor: pointer;
                &:hover {
                    color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                    background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.SubColor};
                    border-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                }
            }
            .xin-icon {
                display: flex;
                align-items: center;
                padding: 16px;
                font-size: 10px;
            }
        }
        .logout {
            padding-bottom: 16px;
            cursor: pointer;
        }
    }
`;

export const TagSTY = styled.div`
    border-radius: 4px;
    padding: 4px 8px;
    margin: 4px;
    color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
    background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.SubColor};
    cursor: pointer;

    &.B {
        padding: 6px 20px;
    }

    &:hover {
        color: #fff;
        background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
    }
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        &.B {
            padding: 6px 12px;
        }
    }
`;

export const NotificationsSTY = styled.div`
    height: 400px;
    overflow-y: scroll;
    .notify-card {
        height: 80px;
        width: 100%;
        min-width: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding: 11px;
        margin-bottom: 5px;
        border-bottom: 2px solid;
        border-bottom-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.SubColor};
        cursor: pointer;
        p {
            margin: 0;
        }
        .notify-title {
            max-height: 40px;
            display: -webkit-box;
            color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            overflow: hidden;
            white-space: normal;
            text-overflow: ellipsis;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            &:hover {
                font-weight: bold;
                text-decoration: underline;
            }
        }
        .notify-time {
            color: #666;
            font-size: 12px;
        }
    }
`;
export default {};
