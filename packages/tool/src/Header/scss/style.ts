import styled from 'styled-components';
import { _DeviceWidth } from '../../theme';

const _Theme = (property: string, rule: { [key in string]: string }, prefix?: string) => {
    return Object.keys(rule).map(
        keyname => `.${prefix}${keyname}{
        ${property}: ${rule[keyname]};
    }`,
    );
};

export const HeaderSTY = styled.div`
    position: relative;
    font-family: Microsoft JhengHei;
    background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.HeaderColor || '#fff'};
    box-shadow: 0 2px 2px -2px rgba(0, 0, 0, 0.15);

    &.fixed {
        position: fixed;
        left: 0;
        top: 0;
        min-width: 100vw;
        width: 100%;
        z-index: 30;
    }
    * {
        box-sizing: border-box;
    }
`;

export const BodySTY = styled.div`
    display: grid;
    max-width: 1400px;
    margin: 0 auto;
    grid-template-areas: 'menu logo tool';
    grid-template-columns: auto 1fr auto;
    grid-column-gap: 4px;

    .bold {
        font-weight: bold;
    }

    .flex {
        display: flex;
        flex-wrap: wrap;

        &.c{
            justify-content: center;
            text-align: center;
        }
    }

    ${({ theme }) => _Theme('background-color', { ...theme.color, ...theme.base }, 'bg-')}

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    grid-template-columns: 1fr auto 1fr;

    }
`;

export const MenuSTY = styled.div`
    grid-area: menu;
    padding-left: 8px;
    display: flex;
    align-items: center;
    cursor: pointer;
    .xin-icon {
        color: ${({ theme }) => theme?.XinMediaDesign?.Header?.ButtomColor};
    }

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        display: none;
    }
`;

export const LogoSTY = styled.a<{ size?: number }>`
    grid-area: logo;
    display: flex;
    max-width: 150px;
    padding: 0px 4px;
    cursor: pointer;

    > .xin-icon {
        display: flex;
        align-items: center;
        justify-items: center;
        color: ${({ theme }) => theme?.XinMediaDesign?.Header?.LogoColor};
        font-size: ${({ size }) => `${size - 8}px`};
        height: auto;
    }

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        padding: 0 16px;
        > .xin-icon {
            font-size: ${({ size }) => `${size}px`};
        }
    }
`;

export const ListSTY = styled.div`
    height: 45px;
    z-index: 2;
    display: none;
    justify-content: center;

    &::after {
        content: '';
        position: absolute;
        left: 0;
        height: 45px;
        width: 100%;
        background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.ListColor};
        border-top: solid 1px #0001;
        z-index: -1;
    }

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        display: flex;
        grid-column: 1/-1;
    }
`;

export const ItemListSTY = styled.div`
    position: relative;
    display: grid;
    grid-auto-flow: column;
    grid-gap: 4px;
    height: 45px;

    > div {
        position: relative;
        display: flex;
        align-items: center;
        font-size: 16px;
        color: #222;
        user-select: none;
        cursor: pointer;

        &:nth-child(n + 2)::before {
            content: '/';
            color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
        }

        &:hover,
        &.active {
            color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            &::after {
                content: '';
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                height: 3px;
                background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            }
        }
        &:hover {
            .board {
                display: grid;
            }
        }

        .name {
            display: flex;
            align-items: center;
            height: 100%;
            padding: 0 8px;
        }
    }
`;

export const BoardSTY = styled.div`
    position: absolute;
    top: 100%;
    display: none;
    color: #222;
    grid-template-columns: 200px auto;
    border-radius: 3px;
    border: solid 1px ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
    background-color: #fff;
    font-size: 14px;

    .board_main {
        background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.SubColor};
        padding: 15px 0px;
        > div {
            padding: 10px 12px 10px 30px;
            display: grid;
            grid-template-columns: 1fr auto;
            align-items: center;
            .xin-icon {
                display: flex;
                align-items: center;
                color: gray;
                width: 10px;
                height: 26px;
            }

            &:hover,
            &.action {
                color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                background-color: rgba(255, 255, 255, 0.6);
            }
        }
    }
    .board_sub {
        padding: 20px 30px;
        > div {
            padding: 8px 16px;
            white-space: nowrap;
            &:hover {
                color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
            }
        }
    }
`;

export const DrawerSTY = styled.div`
    .Drawer_box {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        overflow-y: scroll;
        width: 205px;

        .listWrap {
            user-select: none;
            background-color: #ffffff;

            .listItem {
                width: 100%;
                height: 40px;
                font-size: 16px;
                font-weight: bold;
                line-height: 40px;
                padding: 0 20px 0 15px;
                box-sizing: border-box;
                border-bottom: 1px solid #dddddd;
                overflow: hidden;

                .xin-icon {
                    float: right;
                    height: 100%;
                }
                .right {
                    float: right;
                    height: 100%;
                    transform: rotate(-90deg);
                }
                &.active {
                    color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                    border-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};

                    .xin-icon {
                        > svg {
                            transform: rotate(180deg);
                        }
                    }
                }
            }
            .secondLayer {
                background-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.SubColor};
                .listItem {
                    border-color: ${({ theme }) => theme?.XinMediaDesign?.Header?.PrimaryColor};
                }
            }
        }
        .footer {
            padding: 10px;
        }
    }
    .Drawer {
        display: none;
        &.show {
            display: block;
        }
    }
    .displayNone {
        display: none;
    }
`;

export default {};
