import * as React from 'react';
import cx from 'classnames';
import Menu, { I_Menu, I_Menu_Ref } from './component/menu';
import Logo, { I_Logo } from './component/logo';
import List, { I_List } from './component/list';
import Tool, { F_Tool } from './component/tool';
import { BodySTY, HeaderSTY } from './scss/style';
import HeaderContext, { I_ListDate } from './context';

export interface I_Common {
    style?: React.CSSProperties;
    className?: string;
}
interface I_Main extends I_Common {
    fixed?: boolean;
    login?: boolean;
    leftList?: I_ListDate;
    currentPage?: string[];
}
interface Header extends React.FC<I_Main> {
    Menu?: React.ForwardRefExoticComponent<I_Menu & React.RefAttributes<I_Menu_Ref>>;
    Logo?: React.FC<I_Logo>;
    List?: React.FC<I_List>;
    Tool?: F_Tool;
}

export const HeaderWrap: Header = props => {
    const { fixed, style, children, login, leftList, currentPage } = props;

    const bodyRef = React.useRef<HTMLDivElement>(null);
    const [scroll, setScroll] = React.useState(false);

    const _Scroll = () => {
        const bodyHeight = bodyRef.current?.offsetHeight || 0;
        const screenScrollY = window.scrollY;

        if (bodyHeight < screenScrollY) {
            !scroll && setScroll(true);
        } else {
            setScroll(false);
        }
    };

    React.useEffect(() => {
        if (fixed) {
            document.addEventListener('scroll', _Scroll);
            return () => {
                document.removeEventListener('scroll', _Scroll);
            };
        } else {
            return () => console.log('object 88');
        }
    }, []);

    return (
        <>
            <HeaderSTY className={cx({ fixed: scroll })}>
                <BodySTY className={cx('HeaderWrap')} style={style} ref={bodyRef}>
                    <HeaderContext.Provider value={{ login: login, data: leftList, current: currentPage }}>{children}</HeaderContext.Provider>
                </BodySTY>
            </HeaderSTY>
            {scroll && <div style={{ height: `${bodyRef.current?.offsetHeight || 0}px` }} />}
        </>
    );
};

HeaderWrap.Menu = Menu;
HeaderWrap.Logo = Logo;
HeaderWrap.Tool = Tool;
HeaderWrap.List = List;

export default HeaderWrap;
