import * as React from 'react';

interface I_Child {
    main: string;
    sub?: string[];
}
interface I_ListItem {
    name: string;
    children?: I_Child[];
}
export interface I_ListDate {
    data?: I_ListItem[];
    onClick?: (e: string[]) => void;
    render?: (e: string[]) => React.ReactNode;
}

interface I_HeaderCtx {
    login: boolean;
    data?: I_ListDate;
    current?: string[];
}

const HeaderState = {
    login: false,
};

const HeaderContext = React.createContext<I_HeaderCtx>(HeaderState);

export default HeaderContext;
