import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { MenuSTY, DrawerSTY } from '../scss/style';
import { Icon, Drawer } from '@kade-design/core';
import HeaderContext from '../context';

interface I_Data {
    name: string;
    onClick: (name: string) => void;
    child?: I_Data[];
}
export interface I_Menu extends I_Common {
    data?: I_Data;
    footer?: React.ReactNode;
    onClick?: (nameArr: string[]) => void;
    remainOpen?: boolean;
    children?: React.ReactNode;
}

export interface I_Menu_Ref {
    closeDrawer: () => void;
}

const Menu: React.ForwardRefRenderFunction<I_Menu_Ref, I_Menu> = (props, ref) => {
    const { style, children, className, footer } = props;
    const [visible, setvisible] = React.useState(false);
    // context
    const { data: leftList, current } = React.useContext(HeaderContext);

    const toggleMainArr = new Array(leftList?.data.length).fill(false);
    const [toggleMain, setToggleMain] = React.useState<boolean[]>(toggleMainArr);
    const [toggleSub, setToggleSub] = React.useState<boolean[]>([]);

    const handleOpen = (open: boolean) => {
        const newArr = new Array(leftList?.data.length).fill(false);
        open ? '' : setToggleMain(newArr);
    };

    React.useImperativeHandle(ref, () => ({
        closeDrawer: () => {
            setvisible(false);
        },
    }));

    React.useEffect(() => {
        if (current && leftList) {
            const newArr = new Array(leftList.data.length).fill(false);
            let active: number;
            leftList?.data?.forEach((item, idx) => {
                if (item.name === current[0]) {
                    newArr[idx] = true;
                    active = idx;
                }
            });
            setToggleMain(newArr);

            if (current.length > 1) {
                const subArr = new Array(leftList.data[active].children.length).fill(false);
                leftList?.data[active]?.children.forEach((item, idx) => {
                    if (item.main === current[1]) {
                        subArr[idx] = true;
                    }
                });
                setToggleSub(subArr);
            }
        }
    }, [leftList, current]);

    return (
        <>
            <MenuSTY
                className={cx('HeaderMenu', { [className]: className })}
                onClick={() => {
                    setvisible(true);
                    handleOpen(props.remainOpen);
                }}
                style={style}
            >
                <Icon type={'menu-2'} />
                {children}
            </MenuSTY>
            <DrawerSTY className="DrawerSTY">
                <Drawer visible={visible} onClose={() => setvisible(false)} placement={'left'}>
                    <div className="listWrap">
                        {leftList?.data?.map((mainItem, idx) => {
                            //第一層
                            return (
                                <React.Fragment key={`first_${idx}`}>
                                    <div
                                        className={cx('listItem', { active: toggleMain[idx] })}
                                        onClick={() => {
                                            const newArr = new Array(leftList.data.length).fill(false);
                                            newArr[idx] = !toggleMain[idx];
                                            if (current && mainItem.name === current[0]) newArr[idx] = true;
                                            setToggleMain(newArr);
                                            const subArr = [...toggleSub];
                                            subArr.fill(false);
                                            setToggleSub(subArr);
                                            // 當沒有下一層時才會執行callback;
                                            if (!mainItem.children) {
                                                leftList.onClick && leftList.onClick([mainItem.name]);
                                                setvisible(false);
                                            }
                                        }}
                                    >
                                        {mainItem.name}
                                        {!mainItem.children && <Icon type="arrow-down" className="right" fill="#999999" />}
                                        {mainItem.children && <Icon type="arrow-down" fill="#999999" />}
                                    </div>
                                    <div className={cx({ displayNone: !toggleMain[idx] }, 'secondLayer')}>
                                        {mainItem.children &&
                                            mainItem?.children.map((subItem, idx2) => {
                                                // 第二層
                                                return (
                                                    <div key={`second_${idx2}`} className={cx('subWrap')}>
                                                        <div
                                                            key={idx2}
                                                            className={cx('listItem', { active: toggleSub[idx2] })}
                                                            onClick={() => {
                                                                const newSubArr = new Array(mainItem.children.length).fill(false);
                                                                newSubArr[idx2] = true;
                                                                if (current && current.length > 1 && subItem.main === current[1]) newSubArr[idx2] = true;
                                                                setToggleSub(newSubArr);
                                                                const clickedItem = [mainItem.name, subItem.main];
                                                                if (!subItem.sub) {
                                                                    leftList.onClick && leftList.onClick(clickedItem);
                                                                    setvisible(false);
                                                                }
                                                            }}
                                                        >
                                                            {subItem.main}
                                                        </div>
                                                        <div className={cx('finalItemWrap', { displayNone: !toggleSub[idx2] || !subItem.sub })} style={{ border: 'solid green 1px' }}>
                                                            {subItem.sub
                                                                ? subItem?.sub.map((finalItem, idx3) => {
                                                                      // 第三層
                                                                      return (
                                                                          <div
                                                                              key={`third_${idx3}`}
                                                                              className="finalItem"
                                                                              onClick={() => {
                                                                                  const clickedItem = [mainItem.name, subItem.main, finalItem];
                                                                                  leftList.onClick && leftList.onClick(clickedItem);
                                                                                  setvisible(false);
                                                                              }}
                                                                          >
                                                                              {finalItem}
                                                                          </div>
                                                                      );
                                                                  })
                                                                : null}
                                                        </div>
                                                    </div>
                                                );
                                            })}
                                    </div>
                                </React.Fragment>
                            );
                        })}
                    </div>
                    {!!footer && <div className="footer">{footer}</div>}
                </Drawer>
            </DrawerSTY>
        </>
    );
};

export default React.forwardRef(Menu);
