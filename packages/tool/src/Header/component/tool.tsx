import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { ToolSTY, BodySTY, BoxSTY, TagSTY, NotificationsSTY } from '../scss/tool';
// import { GridSTY } from '../scss/grid';
import { Avatar, ClickOutside, Icon, Badge } from '@kade-design/core';
import HeaderContext from '../context';

const { Trigger, Window } = ClickOutside;

interface I_SVGBox {
    type: string | React.ReactNode;
    show?: 'Common' | 'Logout' | 'Login';
    onClick?: () => void;
    order?: number;
    count?: number;
    dot?: boolean;
}

const SVGBox: React.FC<I_SVGBox> = props => {
    const { type, children, order, count, dot, show = 'Common', onClick } = props;
    const { login } = React.useContext(HeaderContext); // true => Login ; false => Logout
    const Isicon = typeof type === 'string';

    const state = login ? 'Login' : 'Logout';
    const icon = (
        <Badge count={count} dot={dot}>
            <Icon type={type as string} />
        </Badge>
    );
    return (
        <ClickOutside className={cx({ hover: !Isicon, none: show != 'Common' && show !== state })} scrollClose style={{ gridColumnStart: order }}>
            <Trigger toggle={Isicon} onClick={() => onClick && onClick()}>
                {Isicon ? icon : type}
            </Trigger>
            {children && (
                <Window exclude={['input']} close>
                    {children}
                </Window>
            )}
        </ClickOutside>
    );
};
export interface I_Date {
    data: string[];
    onClick?: (e: string) => void;
}
export interface I_Tool extends I_Common {
    avatar?: string;
    nickName?: string;
    level?: string;
    identity?: string;
    showBell?: boolean;
    newNotificationCount?: number;
    searchPlaceholder?: string;
    point?: number;
    memberMenu?: I_Date;
    serviceMenu?: I_Date;
    hotTag?: string[];
    logout?: () => void;
    login?: () => void;
    tomore?: () => void;
    searchCallBack?: (input: string) => void;
    tagOnClick?: (tagName: string) => void;
    otherWeb?: { data: string[]; onClick: (webName: string) => void };
    notifications?: { msg: string; created_at: string; onClick: () => void }[];
}

export interface F_Tool extends React.FC<I_Tool> {
    SVGBox?: typeof SVGBox;
}

const Tool: F_Tool = props => {
    const {
        showBell,
        newNotificationCount,
        notifications,
        style,
        children,
        avatar,
        nickName,
        point,
        identity,
        logout,
        hotTag,
        serviceMenu,
        login,
        otherWeb,
        tomore,
    } = props;

    return (
        <ToolSTY className={'HeaderTool'} style={style}>
            <BodySTY>
                {children}
                {/* 搜尋&標籤 */}
                <SVGBox type="xin-search">
                    <BoxSTY>
                        <div className={cx('bg-primary_2', 'bold')}></div>
                        <div>
                            全站熱搜標籤
                            <div className={cx('hotTag', 'bg-white', 'flex')}>
                                {hotTag.map((name, index) => (
                                    <TagSTY
                                        key={`Tool-otherWeb-${index}`}
                                        onClick={() => {
                                            props.tagOnClick && props.tagOnClick(name);
                                        }}
                                    >
                                        {`# ${name}`}
                                    </TagSTY>
                                ))}
                            </div>
                        </div>
                    </BoxSTY>
                </SVGBox>
                {/* 服務 */}
                <SVGBox type="xin-more">
                    <BoxSTY>
                        <div className={cx('bg-primary_2', 'flex c', 'bold')}>欣傳媒服務</div>
                        <div className={cx('bg-white', 'flex c')}>
                            {otherWeb?.data.map((webName, index) => (
                                <TagSTY
                                    key={`Tool-otherWeb-${index}-key`}
                                    className={cx('B', 'bold')}
                                    onClick={() => {
                                        otherWeb.onClick && otherWeb.onClick(webName);
                                    }}
                                >
                                    {webName}
                                </TagSTY>
                            ))}
                        </div>
                        {!!serviceMenu && (
                            <div className={cx('flex', 'serviceMenu')}>
                                {serviceMenu?.data.map((name, index) => (
                                    <div key={`Tool-serviceMenu-${index}-${name}`} onClick={() => serviceMenu.onClick(name)}>
                                        {name}
                                    </div>
                                ))}
                            </div>
                        )}
                    </BoxSTY>
                </SVGBox>
                {/* 通知鈴鐺 */}
                {showBell ? (
                    <SVGBox type="bell" show="Common" dot={true} count={newNotificationCount}>
                        <BoxSTY>
                            <div className={cx('bg-primary_2', 'flex c', 'bold')}>
                                <div>您的通知</div>
                            </div>
                            {notifications?.length ? (
                                <NotificationsSTY>
                                    {notifications.map((notification, index) => (
                                        <div key={index} className={cx('bg-white', 'notify-card')} onClick={notification.onClick}>
                                            <p className={cx('notify-title')}>{notification.msg}</p>
                                            <p className={cx('notify-time')}>{notification.created_at}</p>
                                        </div>
                                    ))}
                                </NotificationsSTY>
                            ) : null}
                            <div className={cx('to-more', 'bg-white', 'flex c')} onClick={tomore}>
                                看所有通知
                            </div>
                        </BoxSTY>
                    </SVGBox>
                ) : null}
                {/* 使用者頭像 */}
                <SVGBox type={<Avatar size={30} src={avatar} title={nickName} />} show="Login">
                    <BoxSTY className="memberBox">
                        <div className={cx('bg-primary_2', 'topWrap')}>
                            <Avatar size={70} src={avatar} title={nickName} />
                            <div className={cx('nickName', 'flex c', 'bold')}>{nickName}</div>
                            <div className={cx('idWrap', 'flex c', 'bold')}>
                                <div className="identity">{identity}</div>
                                {/* 需求更新身分先mark起來 */}
                                {/* <Divider vertical />
                                    <div className="level">{level}</div> */}
                            </div>
                        </div>
                        <div className={cx('travelPWrap', 'flex c')}>
                            旅遊金
                            <span className={cx('point', 'bold')}>{point}</span>點
                        </div>
                        {/* 需求更新:先mark起來 */}
                        {/* <GridSTY gap={4} className={cx('memberMenu')}>
                                {memberMenu?.data.map((name, index) => {
                                    return (
                                        <GridSTY columns={1} gridColumns={'1fr'} className={cx('bg-white')} onClick={() => memberMenu.onClick(name)} key={`memberMenu-${index}`}>
                                            <Icon type="arrow-right-2" />
                                            {name}
                                        </GridSTY>
                                    );
                                })}
                            </GridSTY>  */}
                        <div className={cx('logout', 'flex c')} onClick={() => logout()}>
                            登出
                        </div>
                    </BoxSTY>
                </SVGBox>
                {/* 登入 */}
                <SVGBox
                    type={
                        <div className={cx('login', 'flex c')} onClick={() => login()}>
                            登入
                        </div>
                    }
                    show="Logout"
                />
            </BodySTY>
        </ToolSTY>
    );
};

Tool.defaultProps = {
    memberMenu: { data: ['我的部落格', '我的部落格後台', '我的訂閱', '我的名片'], onClick: e => console.log(e) },
    // serviceMenu: { data: ['關於我們', '常見問題', '會員條款', '廣告合作', '客服中心', '聯絡我們', '隱私權政策'], onClick: e => console.log(e) },
    otherWeb: {
        data: ['欣部落', '欣講堂', '欣嚴選', '欣會員', '欣新聞', '交易條款'],
        onClick: e => console.log(e),
    },

    hotTag: ['單車旅遊', '環遊世界旅行', '武漢疫情世界旅行', '單車旅遊', '單車旅遊'],
    nickName: '無名',
    point: 0,
    level: '讀者',
    identity: '登機中',
    searchPlaceholder: '搜尋商品名稱',
    logout: () => console.log('logout'),
    login: () => console.log('login'),
    tomore: () => console.log('tomore'),
};

Tool.SVGBox = SVGBox;

export default Tool;
