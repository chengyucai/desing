import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { Icon } from '@kade-design/core';
import { ListSTY, ItemListSTY, BoardSTY } from '../scss/style';
import { GridSTY } from '../scss/grid';
import HeaderContext from '../context';

export type I_List = I_Common;

const List: React.FC<I_List> = props => {
    const { style, className } = props;
    const [boardIndex, setboardIndex] = React.useState<number[]>([]);

    const { data: leftList, current } = React.useContext(HeaderContext);

    React.useEffect(() => {
        if (boardIndex.length === 0 && leftList?.data) {
            setboardIndex(pre => pre.concat(new Array(leftList.data.length).fill(0)));
        }
    }, [leftList]);

    const _board = (_props?: typeof leftList.data[0], Index?: number, _onClick?: typeof leftList.onClick, defActive?: string) => {
        if (_props === undefined) return null;
        const { children } = _props;

        const onlyMain = children.every(item => !item?.sub || item.sub.length === 0);

        const Main = children?.map((item, index) => {
            const { main } = item;

            return (
                <div
                    className={cx({ action: (!onlyMain && boardIndex[Index] === index) || (defActive && main === defActive) }, 'bold')}
                    onClick={() => onlyMain && _onClick([_props.name, main])}
                    onMouseEnter={() =>
                        !onlyMain &&
                        setboardIndex(pre => {
                            pre[Index] = index;
                            return [...pre];
                        })
                    }
                    key={`HeaderList-board-main-${index}`}
                >
                    {main}
                    {!onlyMain && <Icon type="arrow-right-2" />}
                </div>
            );
        });

        const Sub = children[boardIndex[Index]]?.sub?.map((name, index) => {
            return (
                <div onClick={() => _onClick([_props.name, children[boardIndex[Index]].main, name])} key={`HeaderList-board-sub-${index}`}>
                    {name}
                </div>
            );
        });

        return (
            <BoardSTY className={'board'}>
                <div className={'board_main'}>{Main}</div>
                {!onlyMain && (
                    <GridSTY rows={2} gridRows={'1fr'}>
                        <GridSTY columns={2} className={'board_sub'}>
                            {Sub}
                        </GridSTY>
                    </GridSTY>
                )}
            </BoardSTY>
        );
    };

    const _list = (_props?: typeof leftList, defActive?: string[]) => {
        if (_props === undefined || _props.data === undefined) return null;
        const { data, onClick } = _props;

        const list = data.map((item, index) => {
            const isActive = defActive && item.name === defActive[0];
            return (
                <div className={cx('bold', { active: isActive })} key={`HeaderList-${index}`}>
                    {_props?.render ? (
                        _props?.render([item.name])
                    ) : (
                        <div className={cx('name')} onClick={() => onClick([item.name])}>
                            {item.name}
                        </div>
                    )}
                    {item?.children && _board(item, index, onClick, isActive && (defActive[1] || null))}
                </div>
            );
        });

        return <ItemListSTY>{list}</ItemListSTY>;
    };

    return (
        <ListSTY className={cx('HeaderList', { [className]: className })} style={style}>
            {_list(leftList, current)}
        </ListSTY>
    );
};

export default List;
