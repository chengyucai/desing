# `HeaderWrap`

撰寫`README.md`時，請將模組的使用規範規則與步驟規範清楚，使可讀性提高。


`README.md`中包含定義與說明`HeaderWrap`組件所需的功能。它通常與**專案**一起產出，請用簡單的方式寫出易讀性高的文章，~~請統一且美觀的呈現~~。

**Note:** 討論專案中需要哪些功能，或是說明要修改的內容有哪些。

## Example Usage

```js
var HeaderWrap = require('HeaderWrap');
```
