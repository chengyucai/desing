import styled from 'styled-components';
import { _DeviceWidth } from '../../theme';

export const FooterSTY = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 160px;
    padding: 12px;
    background-color: ${({ theme }) => theme?.XinMediaDesign?.Footer?.BackgroundColor || '#f2f2f2'};
    box-sizing: border-box;

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const BodySTY = styled.div<{ maxWidth?: string }>`
    ${({ maxWidth }) => maxWidth && `max-width:${maxWidth};width: -webkit-fill-available;`}
    display: grid;

    grid-gap: 8px;
    grid-template-areas: 'logo' 'list' 'content';
    grid-template-rows: auto 1fr;
    text-align: center;
    justify-items: center;

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        &:not(.center) {
            text-align: left;
            justify-items: flex-start;
        }

        &.classic {
            grid-gap: 8px;
            grid-template-areas: 'logo list' 'content list';
        }
        &.horizontal {
            grid-gap: 8px 0;
            grid-template-areas: 'logo content list';
        }
    }
`;

export const LogoSTY = styled.div<{ size?: string }>`
    grid-area: logo;
    display: flex;
    align-items: center;
    > .xin-icon {
        display: flex;
        align-items: center;
        color: ${({ theme }) => theme?.XinMediaDesign?.Footer?.IconColor || '#c2c1c1'};
        font-size: ${({ size }) => size};
    }
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const ContentSTY = styled.div`
    grid-area: content;

    display: grid;
    grid-row-gap: 8px;
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const ContentMainSTY = styled.div`
    display: grid;
    align-items: flex-end;
    font-size: 16px;
    color: ${({ theme }) => theme?.XinMediaDesign?.Footer?.ContentMainColor || '#000000'};
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const ContentSubSTY = styled.div`
    font-size: 14px;
    color: ${({ theme }) => theme?.XinMediaDesign?.Footer?.ContentSubColor || '#666666'};

    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
    }
`;

export const ListSTY = styled.div`
    grid-area: list;
    display: grid;
    grid-gap: 8px 16px;
    width: fit-content;
    color: ${({ theme }) => theme?.XinMediaDesign?.Footer?.ListColor || '#666666'};

    > div {
        display: grid;
        grid-auto-flow: column;
        grid-gap: 4px 12px;
        font-size: 14px;
        > div.ListTitle {
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 4px;
        }
        > div:not(.ListTitle) {
            cursor: pointer;
        }
    }
    @media screen and (min-width: ${({ theme }) => _DeviceWidth(theme).laptop}) {
        grid-auto-flow: column;
        > div {
            grid-auto-flow: initial;
        }
    }
`;

export default {};
