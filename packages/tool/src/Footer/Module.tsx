import * as React from 'react';

import cx from 'classnames';
import { BodySTY, FooterSTY } from './scss/style';

import FooterContext from './context';
import Logo, { I_Logo } from './component/logo';
import Content, { FC_Footer } from './component/content';
import List, { I_List } from './component/list';

export interface I_Common {
    style?: React.CSSProperties;
    className?: string;
}
interface I_Main extends I_Common {
    login?: boolean;
    typeset?: 'horizontal' | 'classic' | 'center';
    maxWidth?: string;
}
interface I_Footer extends React.FC<I_Main> {
    Logo?: React.FC<I_Logo>;
    Content?: FC_Footer;
    List?: React.FC<I_List>;
}

export const Footer: I_Footer = props => {
    const { style, children, login, typeset, maxWidth } = props;

    return (
        <FooterSTY className={cx('Footer')}>
            <BodySTY className={cx('FooterBody', { [typeset]: typeset })} style={style} maxWidth={maxWidth}>
                <FooterContext.Provider value={{ login: login }}>{children}</FooterContext.Provider>
            </BodySTY>
        </FooterSTY>
    );
};

Footer.Logo = Logo;
Footer.Content = Content;
Footer.List = List;

Footer.defaultProps = {
    typeset: 'classic',
};

export default Footer;
