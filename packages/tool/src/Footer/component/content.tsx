import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { ContentSTY, ContentMainSTY, ContentSubSTY } from '../scss/style';

interface I_Content extends I_Common {
    onClick?: () => void;
}

export interface FC_Footer extends React.FC<I_Content> {
    Main?: typeof ContentMainSTY;
    Sub?: typeof ContentSubSTY;
}

const Content: FC_Footer = props => {
    const { style, children, className, onClick } = props;

    return (
        <ContentSTY className={cx('FooterContent', { [className]: className })} onClick={() => onClick()} style={style}>
            {children}
        </ContentSTY>
    );
};

Content.defaultProps = {
    onClick: () => console.log('Content'),
};

Content.Main = ContentMainSTY;
Content.Sub = ContentSubSTY;
export default Content;
