import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { LogoSTY } from '../scss/style';
import { Icon } from '@kade-design/core';

export interface I_Logo extends I_Common {
    logo?: React.ReactNode | string;
    size?: string;
    onClick?: () => void;
}

const Logo: React.FC<I_Logo> = props => {
    const { style, children, className, logo, size, onClick } = props;
    const Icon_P = typeof logo === 'string' ? { type: logo } : { component: logo };

    return (
        <LogoSTY className={cx('FooterLogo', { [className]: className })} onClick={() => onClick()} style={style} size={size}>
            {children ? children : <Icon {...Icon_P} />}
        </LogoSTY>
    );
};

Logo.defaultProps = {
    logo: 'photo',
    size: '40px',
    onClick: () => console.log('Logo'),
};

export default Logo;
