import * as React from 'react';
import cx from 'classnames';
import { I_Common } from '../Module';
import { ListSTY } from '../scss/style';

export interface I_List extends I_Common {
    onClick?: (v: string) => void;
    data?: {
        name?: string;
        children: {
            name: string;
            herf: string;
        }[];
        onClick?: (v: string) => void;
    }[];
}

const List: React.FC<I_List> = props => {
    const { data, style, children, className, onClick } = props;

    if (!data?.length) return null;

    return (
        <ListSTY className={cx('FooterList', { [className]: className })} style={style}>
            {data?.map((item, index) => {
                const { name, children } = item;
                return (
                    <div key={`FooterList_ListTitle_${index}`}>
                        {name && <div className="ListTitle">{name}</div>}
                        {children.map((sub, index) => (
                            <div onClick={() => (item?.onClick ? item.onClick(sub.herf) : onClick(sub.herf))} key={`FooterList_ListSub_${index}`}>
                                {sub.name}
                            </div>
                        ))}
                    </div>
                );
            })}
            {children}
        </ListSTY>
    );
};

List.defaultProps = {
    onClick: e => window.open(e, '_blank'),
};
export default List;
