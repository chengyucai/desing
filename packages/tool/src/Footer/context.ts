import * as React from 'react';

const HeaderState = {
    login: false,
};

const FooterContext = React.createContext(HeaderState);

export default FooterContext;
