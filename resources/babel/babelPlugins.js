const { addNamed } = require('@babel/helper-module-imports');

module.exports = function babelPlugins() {
    return {
        visitor: {
            Program: {
                enter(path, state) {
                    state.svgSet = new Set();
                },
                exit(path, state) {
                    state.svgSet.forEach(svg => {
                        addNamed(path, `${svg}`, `@kade-design/tool/lib/assets/${svg}.svg`);
                    });
                },
            },
            JSXElement(path, state) {
                const { attributes } = path.node.openingElement;
                if (path.node.openingElement.name.name !== 'Icon') return;
                attributes.forEach(({ name, value }) => {
                    // 判断 name.name 是否等于 "type" 或者是其他设置好的关键词
                    if (name.name === 'type') state.svgSet.add(value.value);
                });
            },
        },
    };
};
