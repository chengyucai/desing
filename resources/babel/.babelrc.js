function firstUpperCase(str) {
    return str.toLowerCase().replace(/( |^)[a-z]/g, L => L.toUpperCase());
}

const presets = ['next/babel'];
const plugins = [
    [
        'import',
        {
            libraryName: '@kade-design/tool',
            libraryDirectory: 'lib/src',
            camel2UnderlineComponentName: false,
            camel2DashComponentName: false,
            style: function(name) {
                return `${firstUpperCase(name)}/index.css`;
            },
        },
        '@kade-design/tool',
    ],
    [
        'import',
        {
            libraryName: '@xin-design/icons',
            libraryDirectory: 'lib/src',
            camel2UnderlineComponentName: false,
            camel2DashComponentName: false,
            style: function(name) {
                return `@xin-design/icons/lib/index.css`;
            },
            customName: name => {
                if (name === 'createSvg') return `${name}.js`;
                return `@xin-design/icons/lib/src/${name.slice(0, name.length - 4)}.js`;
            },
        },
        '@xin-design/icons',
    ],
    './babelPlugins.js',
];

module.exports = { presets, plugins };
