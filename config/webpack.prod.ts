const path = require('path');
const merge = require('webpack-merge');
const root = path.resolve(__dirname, '../');

const env = require(path.resolve(root, 'config/env'));
const base = require(path.resolve(root, 'config/webpack.base'));

module.exports = merge(base, {
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.(svg|ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani)$/,
                exclude: [/(node_modules)/, ...env.svg_loader_path],
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8310,
                        },
                    },
                ],
            },
        ],
    },
    externals: {
        react: {
            root: 'React',
            commonjs2: 'react',
            commonjs: 'react',
            amd: 'react',
        },
        'react-dom': {
            root: 'ReactDOM',
            commonjs2: 'react-dom',
            commonjs: 'react-dom',
            amd: 'react-dom',
        },
    },
});

export {};
