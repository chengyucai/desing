const path = require('path');
const root = path.resolve(__dirname, '../');
const resolveTsconfigPathsToAlias = require('./utils/resolveTsconfigPathsToAlias');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{ loader: 'babel-loader' }],
            },
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
            },
            {
                test: /\.s(a|c)ss$/,
                exclude: /(node_modules)/,
                loader: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    // Translates CSS into CommonJS
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[local]--[hash:base64:5]',
                            },
                        },
                    },
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
        ],
    },
    resolve: {
        alias: resolveTsconfigPathsToAlias({
            tsconfigPath: path.resolve(root, 'tsconfig.json'),
            webpackConfigBasePath: './',
        }),
        extensions: ['.ts', '.tsx', '.css', '.scss'],
    },
};

export {};
