const path = require('path');
const merge = require('webpack-merge');
const root = path.resolve(__dirname, '../');
const base = require(path.resolve(root, 'config/webpack.base'));
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(base, {
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ],
});

export {};
