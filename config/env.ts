const path = require('path');
const root = path.resolve(__dirname, '../');

const svg_loader_path = [path.resolve(root, 'packages/components/src/Icon/assets')];

export { svg_loader_path };
