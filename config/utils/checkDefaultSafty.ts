const fs = require('fs');
const path = require('path');
const root = path.resolve(__dirname, '../../');
const newLine = require('os').EOL;

const coreFolderName = 'core';

function chechDefaultSafty(pkgfolder = coreFolderName) {
    const ComponentsContext: any[] = [];
    // fs.readFileSync(path.resolve(root, './packages/components/index.ts'))
    //     .toString()
    //     .split(newLine)
    //     .forEach((line: any) => {
    //         if (line !== '' && typeof line === 'string') IndexContext.push(line.split(' ')[4]);
    //     });

    if (pkgfolder !== coreFolderName) {
        // 引入 core 再 export 出去
        ComponentsContext.push(`export * from '@kade-design/core';`);
    }

    fs.readdirSync(path.resolve(root, `./packages/${pkgfolder}/src`)).forEach((folder: any) => {
        if (folder !== '__mocks__') {
            ComponentsContext.push(`export { default as ${folder} } from './src/${folder}';`);
            ComponentsContext.push(`export * from './src/${folder}';`);
        }
    });

    if (fs.existsSync(path.resolve(root, `./packages/${pkgfolder}/utils`))) {
        ComponentsContext.push(`export * from './utils';`);
    }
    if (fs.existsSync(path.resolve(root, `./packages/${pkgfolder}/styles`))) {
        ComponentsContext.push(`export * from './styles';`);
    }

    fs.writeFileSync(path.resolve(root, `./packages/${pkgfolder}/index.ts`), ComponentsContext.join(newLine) + newLine);
}

module.exports = chechDefaultSafty;
