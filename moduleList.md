# ModuleList

列舉出常用的模組，有些模組可能會相依於其他模組。</br>
注意事項：

* 設計前須將可能會傳入的 Props 列舉出來。
* 設計時須考慮如何讓使用者能夠客自化樣式。

## 通用類

1. 按鈕 Button

2. 圖示 Icon

    * SVG。

3. 觸及外部 ClickOutside

    * 點擊到外部時，觸發傳入的Clallback Func。

4. 標題 Label Title

    * 快速設定標題的階級，ex： H1、H2...等。

5. 分隔線 Divider

    * 可以設定水平垂直，以外層的寬高做為長度準則，自定義長度比例。

## 頁面導航類

1. 麵包屑 Breadcrumb

2. 導航選單 MenuTab

    * 切換到不同頁籤內容。

3. 分頁 Pagination

    * 分成換頁有呼叫API得到新資料，或是資料比數已確定模組自行分頁。

4. 步驟條 Steps

5. 下拉選單 DropDown

    * 與選擇器不同，此選單資料為連結或是切換內容。

6. 錨點 Anchor

7. 回頂部 GoTop

    * 樣式透過 RenderProps 傳入。

## 資料輸入類

1. 表單 Form

2. 輸入框(中/英/數) Input

3. 多選框 CheckBox

4. 單選框 Radio

5. 選日子器 DatePicker

    * 單雙日。
    * 可只選 年/月/日。

6. 選擇器(階層) Select

    * 選單為樹狀結構

7. 自動完成/補字 AutoComponele

## 資料顯示類

1. 頭像 Avatar

2. 徽章 Badge

    * 出現位置(上下左右)以及數量。

3. 摺疊清單/表格 CollapseList

    * 使用者可以自定義表格的標題、表格階層以及內容。
    * 考慮 M 版。

4. 評論 Comment

5. 卡片 Card

    * 有圖片時，圖片的大小比例如何處裡。

6. 圖片 Image

7. 泡泡框 Popover/Tooltip

    * 分點擊出現以及hover出現。

8. 標籤 Tag

    * 傳入陣列資料直接顯示全部。
    * 可設定方向。

9. 橫幅(輪播) Banner

    * 效果。

10. 月曆/行程表 Calendar

    * 根據資料的年月日填入資料。
    * 資料顯示可以 children 或 renderFunc 傳入。

## 反饋

1. 警告/對話框 Alert

    * 可有 Callback 來反應使用者選擇。

2. 側邊攔 Drawer

3. 進度條 Progress

4. 結果 Result

    * 顯示某種事件的成功與否，以及文字訊息。

5. 加載中 Spin

    * 圖片尚未載入可以此作為替代。
    * 可傳入其他 SVG 來替代。

6. 骨架 Skeleton

    * 同上概念。

7. 光箱 LightBox

## 其他

1. 公司地址 Address&Phone
