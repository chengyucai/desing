import fs from 'fs';
import path from 'path';
import shell from 'shelljs';
import chalk from 'chalk';
import _ from 'lodash';

const _assetsPath = './packages/core/src/Icon/assets';
const _buildPath = 'packages/core/src/Icon/src';
const _tplPath = 'packages/core/src/Icon/template.js';

// 轉 svg 成 component
fs.readdirSync(_assetsPath).forEach(file => {
    const _fileFullPath = `${_assetsPath}/${file}`;
    const _fileName = path.basename(_fileFullPath, path.extname(_fileFullPath));

    const _buildFileName = _.upperFirst(_.camelCase(_fileName));

    if (fs.existsSync(`${_buildPath}/${_buildFileName}.tsx`)) {
        console.log(chalk.red(`${_buildFileName} is exist!`));
    } else {
        shell.exec(`npx @svgr/cli --icon --template ${_tplPath} --ext tsx -d "${_buildPath}" ${_fileFullPath}`);
        shell.rm(`${_fileFullPath}`);
    }
});

const _componentBase = './packages/core/src/Icon';
const _componentPath = `${_componentBase}/src`;

// 產生所有 icon 列表
const icons = [];
fs.readdirSync(_componentPath).forEach(file => {
    const _fileFullPath = `${_assetsPath}/${file}`;
    const _fileName = path.basename(_fileFullPath, path.extname(_fileFullPath));

    icons.push(_.kebabCase(_fileName));
});

fs.writeFile(`${_componentBase}/icons.json`, JSON.stringify(icons, null, 4), 'utf8', () => {
    console.log(`updated ${_componentBase}/icons.json!`);
});
