import { configure, addParameters, addDecorator } from '@storybook/react';
import { themes } from '@storybook/theming';
import { withKnobs } from '@storybook/addon-knobs';
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';

// function requireAll(requireContext: any) {
//     return requireContext.keys().map(requireContext);
// }

// function loadStories() {
//     requireAll(require['context']('../previews/common/', true, /\preview.tsx?$/));
// }

addParameters({
    options: {
        theme: themes.normal,
    },
    backgrounds: [
        { name: 'White', value: '#FFF', default: true },
        { name: 'Black', value: '#000' },
    ],
    docs: {
        container: DocsContainer,
        page: DocsPage,
    },
});

addDecorator(withKnobs);

const pkg = process.env.STORYBOOK_PKG;
const loaderFn = () => {
    let req;
    const allExports = [];
    switch (pkg) {
        case 'tool':
            // TODO: 讓其他 pkg 共享 preview storybook 檔案
            // dynamic loading, unavailable in react-native
            req = require.context('../tool', true, /\.stories\.(tsx|mdx)$/);
            break;
        default:
            req = require.context('../previews', true, /\.stories\.(tsx|mdx)$/);
    }
    req.keys().forEach(fname => allExports.push(req(fname)));

    return allExports;
};
configure(loaderFn, module);
