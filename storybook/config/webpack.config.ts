const path = require('path');
const merge = require('webpack-merge');
const root = path.resolve(__dirname, '../../');

const common = require(path.resolve(root, 'config/webpack.dev'));
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const env = require(path.resolve(root, 'config/env'));

const createCompiler = require('@storybook/addon-docs/mdx-compiler-plugin');

function HACK_fileLoader(config: any) {
    //要使用 svg-sprite-loader，如果先透過 file loader 處理 svg 會導致 svg-sprite-loader 處理錯誤
    console.warn('HACK: file loader add exclude.');

    const new_config = config.module.rules.map((rule: any) => {
        if (new RegExp(rule.test).toString() === new RegExp(/\.(svg|ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani|pdf)(\?.*)?$/).toString()) {
            rule.exclude = env.svg_loader_path;
        }
        if (new RegExp(rule.test).toString() !== new RegExp(/\.tsx?$/).toString()) return rule;
        return {};
    });
    config.module.rules = new_config;
    return config;
}
// Export a function. Accept the base config as the only param.
module.exports = async ({ config }: { config: any; mode?: any }) => {
    // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    const HackConfig = merge(common, config);
    const mergeConfig = HACK_fileLoader(HackConfig);

    // Make whatever fine-grained changes you need
    mergeConfig.module.rules.push({
        include: path.resolve(__dirname, '../'),
    });

    mergeConfig.module.rules.push({
        test: /\.tsx?$/,
        use: [
            {
                loader: require.resolve('babel-loader'),
                options: {
                    presets: [require.resolve('babel-preset-react-app')],
                },
            },
            {
                loader: require.resolve('react-docgen-typescript-loader'),
            },
        ],
    });

    mergeConfig.module.rules.push({
        test: /\.svg$/,
        loader: [
            {
                loader: 'svg-sprite-loader',
            },
            {
                loader: 'svgo-loader',
                options: {
                    plugins: [{ removeTitle: true }, { convertColors: { shorthex: false } }, { convertPathData: true }, { cleanupAttrs: true }],
                },
            },
        ],
        include: env.svg_loader_path,
    });
    mergeConfig.plugins.push(new SpriteLoaderPlugin());

    // Return the altered config
    return mergeConfig;
};
