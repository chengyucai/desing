const presets = ['@babel/env', '@babel/preset-typescript'];
const plugins = ['add-module-exports'];

module.exports = {
    presets: presets,
    plugins: plugins,
};
