import CurationImg from '@packages/tool/src/CurationImg';

import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import { color, boolean, text } from '@storybook/addon-knobs';

export default {
    title: 'Tool|CurationImg',
    parameters: {
        component: CurationImg,
        includeStories: [],
    },
};

export const Common = () => {
    const Edit = boolean('Edit', false);
    const src = text('src', undefined);
    return <CurationImg img={src ? { src: src } : undefined} titleStyle={{ color: undefined }} contentEditable={Edit} onChange={v => console.log(v)} />;
};
