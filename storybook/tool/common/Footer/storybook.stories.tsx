import Footer from '@packages/tool/src/Footer';

import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import { color } from '@storybook/addon-knobs';

export default {
    title: 'Tool|Footer',
    parameters: {
        component: Footer,
        includeStories: [],
    },
};

export const Xinmedia = () => {
    const { Logo, Content, List } = Footer;
    const data = [
        {
            name: '欣嚴選',
            children: [
                { name: '關於我們', herf: 'https://www.google.com.tw' },
                { name: '欣傳媒', herf: 'https://www.google.com.tw' },
                { name: '欣講堂', herf: 'https://www.google.com.tw' },
                { name: '欣嚴選', herf: 'https://www.google.com.tw' },
            ],
        },
        {
            name: '使用規章',
            children: [
                { name: '使用條款', herf: 'https://www.google.com.tw' },
                { name: '隱私政策', herf: 'https://www.google.com.tw' },
                { name: '常見問題', herf: 'https://www.google.com.tw' },
                { name: '聯絡我們', herf: 'https://www.google.com.tw' },
            ],
        },
    ];
    return (
        <Footer maxWidth="900px">
            <Logo />
            <Content>
                <Content.Main>台灣台北市114內湖區石潭路151號</Content.Main>
                <Content.Sub>Copyright © 2020 Xinmedia All rights reserved.</Content.Sub>
            </Content>
            <List data={data} />
        </Footer>
    );
};

export const Xinmedia2 = () => {
    const { Logo, Content, List } = Footer;
    const data = [
        {
            children: [
                { name: '關於我們', herf: 'https://www.google.com.tw' },
                { name: '欣傳媒', herf: 'https://www.google.com.tw' },
                { name: '欣講堂', herf: 'https://www.google.com.tw' },
                { name: '欣嚴選', herf: 'https://www.google.com.tw' },
            ],
        },
        {
            children: [
                { name: '使用條款', herf: 'https://www.google.com.tw' },
                { name: '隱私政策', herf: 'https://www.google.com.tw' },
                { name: '常見問題', herf: 'https://www.google.com.tw' },
                { name: '聯絡我們', herf: 'https://www.google.com.tw' },
            ],
        },
    ];
    return (
        <Footer maxWidth="900px">
            <Logo />
            <Content>
                <Content.Main>台灣台北市114內湖區石潭路151號</Content.Main>
                <Content.Sub>Copyright © 2020 Xinmedia All rights reserved.</Content.Sub>
            </Content>
            <List data={data} />
        </Footer>
    );
};

export const Center = () => {
    const { Logo, Content, List } = Footer;

    return (
        <Footer maxWidth="900px" typeset="center">
            <Logo />
            <Content>
                <Content.Main>台灣台北市114內湖區石潭路151號</Content.Main>
                <Content.Sub>Copyright © 2020 Xinmedia All rights reserved.</Content.Sub>
            </Content>
        </Footer>
    );
};

export const Theme = () => {
    const { Logo, Content, List } = Footer;
    const theme = {
        XinMediaDesign: {
            Footer: {
                BackgroundColor: color('BackgroundColor', '#43bebf'),
                IconColor: color('IconColor', '#fff'),
                ListColor: color('ListColor', '#fff'),
                ContentMainColor: color('ContentMainColor', '#fff'),
                ContentSubColor: color('ContentSubColor', '#fff'),
            },
        },
    };

    const data = [
        {
            children: [
                { name: '關於我們', herf: 'https://www.google.com.tw' },
                { name: '欣傳媒', herf: 'https://www.google.com.tw' },
                { name: '欣講堂', herf: 'https://www.google.com.tw' },
                { name: '欣嚴選', herf: 'https://www.google.com.tw' },
            ],
        },
        {
            children: [
                { name: '使用條款', herf: 'https://www.google.com.tw' },
                { name: '隱私政策', herf: 'https://www.google.com.tw' },
                { name: '常見問題', herf: 'https://www.google.com.tw' },
                { name: '聯絡我們', herf: 'https://www.google.com.tw' },
            ],
        },
    ];
    return (
        <ThemeProvider theme={theme}>
            <Footer maxWidth="900px" typeset="center">
                <Logo />
                <List data={data} />
                <Content>
                    <Content.Main>台灣台北市114內湖區石潭路151號</Content.Main>
                    <Content.Sub>Copyright © 2020 Xinmedia All rights reserved.</Content.Sub>
                </Content>
            </Footer>
        </ThemeProvider>
    );
};
