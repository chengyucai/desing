import Header from '@packages/tool/src/Header';

import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import { boolean, color } from '@storybook/addon-knobs';
import './custom.scss';

export default {
    title: 'Tool|Header',
    parameters: {
        component: Header,
        includeStories: [],
    },
};

const theme = {
    XinMediaDesign: {
        Header: {
            PrimaryColor: '#43bebf',
            SubColor: '#dbfbf0',
            LogoColor: '#222',
            LinkColor: '#666',
            ButtomColor: '#222',
            NotifyColor: '#666',
        },
    },
};

export const LIst = () => {
    const { Menu, Logo, Tool, List } = Header;

    const board = [
        { main: '旅遊', sub: ['美食探險家1', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
        { main: '美學', sub: ['美食探險家2', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
        { main: '運動', sub: ['美食探險家3', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
        { main: '攝影', sub: ['美食探險家4', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
        { main: '生活休閒', sub: ['美食探險家5', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
        { main: '產業經濟', sub: ['美食探險家6', '海上郵輪', '自然探險', '世界遺產', '鐵道漫遊', '樂園', '旅行攻略'] },
    ];
    const list = {
        leftList: {
            data: [
                { name: '文章分類', children: board },
                { name: '世界走透透', children: board },
                { name: '專題策展', children: board },
                { name: '活動', children: board },
                { name: '相簿', children: board },
                { name: '賺取旅遊金', children: board },
                { name: '欣BLOG', children: board },
            ],
            onClick: name => console.log('nameYO', name),
        },
    };

    return (
        <ThemeProvider theme={theme}>
            <Header {...list}>
                <Menu remainOpen={true} />
                <Logo></Logo>
                <Tool />
                <List />
            </Header>
        </ThemeProvider>
    );
};

export const Login = () => {
    const { Menu, Logo, Tool, List } = Header;

    const board = [{ main: '旅遊' }, { main: '美學' }, { main: '運動' }, { main: '攝影' }, { main: '生活休閒' }, { main: '產業經濟' }];
    const list = {
        leftList: {
            data: [
                { name: '文章分類', children: board },
                { name: '世界走透透', children: board },
                { name: '專題策展', children: board },
                { name: '活動', children: board },
                { name: '相簿', children: board },
                { name: '賺取旅遊金', children: board },
                { name: '欣BLOG', children: board },
            ],
            onClick: name => console.log('nameYO', name),
        },
    };
    const notifications = new Array(12).fill(null).map(() => {
        return {
            msg: '疫情在家要做什麼？給你花不完的旅遊金，給你花不完的旅遊金疫情在家要做什麼？給你花不完的旅遊金，給你花不完的旅遊金',
            created_at: '02月15日 12:53',
            onClick: () => {
                console.log(123);
            },
        };
    });

    const login = boolean('login', false);
    const tool = {
        nickName: '旅達人',
        point: 9999,
        hotTag: ['咖啡', 'aaa'],
        tagOnClick: v => console.log(v),
        showBell: true,
        newNotificationCount: 19,
        notifications,
    };
    return (
        <ThemeProvider theme={theme}>
            <Header login={login} {...list} fixed>
                <Menu remainOpen={true} />
                <Logo></Logo>
                <Tool
                    {...tool}
                    searchCallBack={searchValue => {
                        console.log('searchValue', searchValue);
                    }}
                    otherWeb={{ data: ['欣新聞', '欣講堂', '欣嚴選', '欣會員', '欣部落'], onClick: webName => console.log('webName', webName) }}
                />
                <List />
            </Header>
        </ThemeProvider>
    );
};

export const XinShop = () => {
    const { Menu, Logo, Tool, List } = Header;

    const board = [{ main: '旅遊' }, { main: '美學' }, { main: '運動' }, { main: '攝影' }, { main: '生活休閒' }, { main: '產業經濟' }];
    const list = {
        leftList: {
            data: [
                { name: '文章分類', children: board },
                { name: '世界走透透', children: board },
                { name: '專題策展', children: board },
                { name: '活動', children: board },
                { name: '相簿', children: board },
                { name: '賺取旅遊金', children: board },
                { name: '欣BLOG', children: board },
            ],
            onClick: name => console.log('nameYO', name),
        },
    };

    const login = boolean('login', false);
    const tool = {
        nickName: '旅達人',
        point: 9999,
        hotTag: ['咖啡', 'aaa'],
        tagOnClick: v => console.log(v),
    };
    return (
        <ThemeProvider theme={theme}>
            <Header login={login} {...list} fixed>
                <Menu remainOpen={true} />
                <Logo></Logo>
                <Tool
                    {...tool}
                    searchCallBack={searchValue => {
                        console.log('searchValue', searchValue);
                    }}
                    otherWeb={{ data: ['欣新聞', '欣講堂', '欣嚴選', '欣會員', '欣部落'], onClick: webName => console.log('webName', webName) }}
                >
                    <Tool.SVGBox type="cart" order={3} count={2} onClick={() => console.log('cart')} show="Login" />
                </Tool>
                <List />
            </Header>
        </ThemeProvider>
    );
};

export const List_Menu = () => {
    const { Menu, Logo, Tool, List } = Header;

    const board = [{ main: '旅遊' }, { main: '美學' }, { main: '運動' }, { main: '攝影' }, { main: '生活休閒' }, { main: '產業經濟' }];
    const list = {
        leftList: {
            data: [
                { name: '文章分類', children: board },
                { name: '世界走透透', children: board },
                { name: '專題策展', children: board },
                { name: '活動', children: board },
                { name: '相簿', children: board },
                { name: '賺取旅遊金', children: board },
                { name: '欣BLOG', children: board },
            ],
            onClick: name => console.log('onClick =>', name),
        },
    };

    return (
        <ThemeProvider theme={theme}>
            <Header {...list} currentPage={['文章分類', '旅遊']} fixed>
                <Menu remainOpen={true} />
                <Logo />
                <Tool />
                <List />
            </Header>
        </ThemeProvider>
    );
};

export const Theme = () => {
    const { Menu, Logo, Tool, List } = Header;

    const board = [{ main: '旅遊' }, { main: '美學' }, { main: '運動' }, { main: '攝影' }, { main: '生活休閒' }, { main: '產業經濟' }];
    const list = {
        leftList: {
            data: [
                { name: '文章分類', children: board },
                { name: '世界走透透', children: board },
                { name: '專題策展', children: board },
                { name: '活動', children: board },
                { name: '相簿', children: board },
                { name: '賺取旅遊金', children: board },
                { name: '欣BLOG', children: board },
            ],
            onClick: name => console.log('onClick =>', name),
        },
    };
    const theme = {
        XinMediaDesign: {
            Header: {
                PrimaryColor: color('PrimaryColor', '#43bebf'),
                SubColor: color('SubColor', '#dbfbf0'),
                LogoColor: color('LogoColor', '#222'),
                LinkColor: color('LinkColor', '#666'),
                ButtomColor: color('ButtomColor', '#222'),
                HeaderColor: color('HeaderColor', '#fff'),
                ListColor: color('ListColor', '#fff'),
            },
        },
    };
    return (
        <ThemeProvider theme={theme}>
            <Header {...list} currentPage={['文章分類', '旅遊']} fixed>
                <Menu remainOpen={true} />
                <Logo />
                <Tool />
                <List />
            </Header>
        </ThemeProvider>
    );
};
