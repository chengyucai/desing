import LightBox from '@packages/core/src/LightBox/index';
import Button from '@packages/core/src/Button';
import * as React from 'react';

// import { text, select, number } from '@storybook/addon-knobs';

import * as style from './custom.scss';

export default {
    title: 'Test(測試類)|LightBox',
    parameters: {
        component: LightBox,
        includeStories: [],
    },
};

export const Common = () => {
    // const props = {};
    const [visible, setvisible] = React.useState<boolean>(false);
    return (
        <div className={style.custom}>
            <Button type="xin" onClick={() => setvisible(!visible)}>
                OPEN
            </Button>
            <LightBox open={visible} ToggleLightBox={setvisible}>
                <div>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae nostrum quibusdam consequatur deleniti ratione minus minima velit dicta
                    rerum sunt cumque, quas nam alias reiciendis a accusamus modi, natus, atque odit maxime veniam possimus provident enim! Amet porro
                    aspernatur consequatur illum, maxime voluptatem optio inventore ducimus facilis architecto nesciunt eligendi dolore est ipsam incidunt esse.
                    Reiciendis, in molestias. Nihil voluptates fugiat dignissimos quia molestiae natus id, hic repellendus ullam neque eveniet, quos aspernatur
                    corporis deleniti iure magnam animi? Voluptate, optio nesciunt earum fugit error culpa modi aliquam fuga quia corrupti cum officiis. Itaque
                    nam incidunt enim sint est dignissimos quos suscipit, qui perferendis exercitationem officia impedit commodi, quibusdam architecto quo ipsum
                    possimus inventore voluptatem, deserunt eos aut temporibus quasi. Corporis odit est optio esse hic, culpa ratione officia at magnam totam
                    magni, corrupti, iste debitis exercitationem itaque atque veniam ullam architecto molestiae cupiditate consequatur temporibus fugiat. Iure
                    voluptas sed amet necessitatibus quas similique exercitationem odio fugit ex harum provident qui, nostrum soluta ab dolor delectus molestiae
                    adipisci illo dolorem veniam aspernatur enim ducimus architecto possimus! Consectetur nihil quia, illo similique non repudiandae officiis
                    sed labore excepturi aliquid distinctio provident fugit rerum, possimus voluptatum impedit enim magni et minima ab aut at. Aut, numquam
                    pariatur optio maiores itaque velit odio perspiciatis atque, saepe similique nesciunt repudiandae facere consequatur debitis ea id rerum est
                    dolorem rem illo aliquam accusantium voluptatum! Ducimus fugit iusto sunt blanditiis dolore, repellendus dicta, eligendi odio natus sit qui
                    consectetur quam consequatur, labore ea ut corporis neque vel? Cum reprehenderit labore autem ducimus assumenda nam repudiandae eaque. Ipsum
                    excepturi incidunt unde officiis ut voluptate laboriosam alias necessitatibus, fugiat facere quasi quas explicabo ipsa ducimus possimus
                    temporibus molestias non cum corporis sapiente inventore recusandae quaerat provident? Optio minus rem cumque explicabo illum doloremque
                    quisquam ipsam iure eum beatae ducimus maiores dolores, totam alias amet quibusdam recusandae deleniti nulla harum, officia placeat enim
                    quae dolor. Laboriosam, possimus aliquam facilis sit dolorum velit, rem corporis corrupti molestias suscipit quae ex, eveniet quis doloribus
                    aperiam ipsam aliquid? Beatae vero, eaque eum fugiat ipsam excepturi id omnis, non ut atque ullam? Non ad voluptas maiores nam tenetur,
                    consequuntur quod voluptatem magni ea, ut commodi eaque dolor qui consectetur est suscipit repudiandae autem asperiores nobis! Accusantium
                    quod voluptate voluptatem temporibus unde! Ut ex doloremque expedita atque nesciunt possimus aliquid cupiditate maxime voluptatem,
                    distinctio voluptatum exercitationem accusantium ipsa minima asperiores similique rem, laborum repellat. Facilis dolorum mollitia debitis
                    eveniet. Voluptatem fuga earum officia possimus rerum, sequi, eveniet sunt, quod nisi non illo ad? Nam id quasi libero quos autem, quidem
                    quis numquam aliquam recusandae amet? Quae sed alias tenetur eveniet dolore nam velit debitis veniam. Temporibus repudiandae neque accusamus
                    consequatur quia ut veritatis suscipit, ab aspernatur harum voluptates eveniet quisquam vero numquam adipisci perferendis recusandae soluta
                    nisi eum sequi? Sit et inventore reprehenderit numquam asperiores. Modi a hic voluptas veniam unde. Est tempore quae aperiam iusto rem
                    voluptas unde omnis fuga, cupiditate minima veritatis ipsa, corrupti modi sapiente eligendi excepturi! Nesciunt maiores voluptas magni
                    eaque.
                </div>
            </LightBox>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio, doloremque unde! Sunt delectus molestiae beatae qui itaque laboriosam
                consectetur! Dolorem molestiae expedita culpa obcaecati aspernatur cumque non iure voluptatum id cum officiis sint mollitia placeat animi, totam
                consequatur ullam ducimus unde dolore ex necessitatibus quisquam aliquam. Facilis, voluptatem neque deserunt similique incidunt id nostrum
                aliquid odit dolore natus animi vitae quibusdam! Soluta veritatis nemo nobis ullam dolorem vitae quibusdam libero expedita animi reprehenderit.
                Molestias doloremque deleniti ipsa qui minus, quod atque ratione, iste a mollitia vel maxime in ab deserunt. Voluptates deleniti architecto
                iusto a laboriosam aliquid? Reprehenderit libero nam maxime aliquid quidem dolorum, molestias harum, beatae laboriosam eos sit. Tenetur aperiam,
                cum similique quibusdam quis eligendi recusandae! Aliquid distinctio suscipit qui, eligendi adipisci est maiores! Doloribus placeat vero enim
                excepturi! Sint at, ab quidem neque architecto temporibus recusandae fugit provident ratione harum officiis ipsum cupiditate, perferendis fuga!
                Beatae explicabo eveniet eaque aut animi quasi, saepe aperiam enim nemo eos. Eaque repellendus illo consectetur officia totam quibusdam tempore,
                odio sapiente doloremque voluptatibus quidem voluptas dolor odit quod doloribus est! Placeat ullam quis sequi quos aliquam et impedit rerum
                laboriosam nisi quod quidem aperiam corporis, quae iure mollitia dolores, quibusdam tenetur similique, consectetur ipsa laudantium error. Fuga
                illo placeat, vero omnis quam eligendi obcaecati assumenda officia? Corporis iusto exercitationem maiores quibusdam harum. Incidunt deserunt
                aspernatur consequuntur ipsam, laboriosam voluptates, praesentium autem iure blanditiis natus et voluptas eum neque ad quo velit, dolor quaerat
                magnam? Expedita vel, voluptatum dolore earum doloribus culpa labore quibusdam beatae nemo ducimus delectus, optio ipsam perferendis quod animi
                nam qui. Unde explicabo ut tempore dicta. Quia, quis nisi. Maxime quod accusantium beatae rem voluptates. Omnis provident natus maiores
                voluptatum. Rerum nostrum, sunt, quod consequatur possimus doloribus aliquam fugiat sed totam unde officia. Ullam architecto nam suscipit culpa,
                quasi doloribus magni expedita dolore, quos magnam, ea laboriosam voluptatum? Sint quia numquam consectetur vero debitis, sunt atque, fugit
                perspiciatis itaque architecto impedit ipsa. Quas quis obcaecati vel sed numquam necessitatibus fugit excepturi deleniti. Suscipit modi iste
                nihil beatae eveniet? Distinctio repellendus mollitia est unde quo numquam pariatur architecto. Saepe quisquam repellat fuga dolore id nemo
                quibusdam possimus tenetur temporibus rerum
            </p>
            <Button type="xin" onClick={() => setvisible(!visible)}>
                OPEN
            </Button>{' '}
            <p>
                Animi ex ab, omnis eius quas dignissimos dicta alias blanditiis nostrum cumque suscipit, fugit consequuntur iusto debitis quisquam! Nemo animi a
                dolorem porro mollitia! At nam voluptatibus laborum itaque debitis possimus sint eos, praesentium id eveniet fugiat, dolorum corrupti est ut
                facere modi maiores odit esse nihil, accusamus porro! Provident molestiae quasi dolores similique omnis, facilis quibusdam cumque debitis amet
                ea velit dignissimos. Repellat officiis officia deserunt autem, commodi non exercitationem iusto consequuntur ad vel repudiandae quaerat
                voluptatem eveniet eaque, necessitatibus odit, eos ipsam dolor saepe ea dolore. Aperiam omnis facilis possimus veniam nisi incidunt adipisci
                repellat, inventore minus, unde vero quaerat tempore rerum cumque, placeat illo! Optio incidunt perspiciatis ipsa iusto eaque corrupti eos. Qui
                nihil dicta natus corporis, rem eligendi optio ipsam quos reprehenderit aliquam ex, facilis esse eveniet molestiae nam architecto a beatae et
                nesciunt eaque, tenetur voluptate ullam id inventore. Assumenda officiis cum explicabo consequatur dolore corporis nihil quam totam porro! A
                porro laboriosam optio culpa obcaecati fugit veritatis odio, doloribus at! Consequuntur iste perspiciatis repudiandae et officiis ipsum pariatur
                dignissimos ducimus dolorem, reprehenderit doloribus veritatis repellat, sint dicta sed, sapiente aspernatur tempore culpa! Temporibus, tempora
                molestiae? Quisquam blanditiis quo autem vitae reiciendis nostrum nam praesentium cupiditate eveniet sapiente iste nesciunt libero dolorem
                possimus culpa illo neque, deleniti dolor exercitationem rem perferendis reprehenderit numquam! Soluta nulla tempore, velit ipsum laborum sint
                iste aliquam provident animi quibusdam, ab pariatur ducimus delectus nihil? Commodi quos, ex maxime aliquid laudantium vitae totam ipsum minima
                impedit corrupti voluptas soluta voluptatem provident, natus facere rem assumenda reiciendis nemo ducimus tenetur officia laborum quibusdam
                exercitationem? Saepe error distinctio modi nulla porro rerum enim quibusdam iure architecto repellat veritatis veniam ipsum maiores,
                necessitatibus minus eos odit, in optio temporibus cupiditate deserunt odio corrupti? Perspiciatis repellat culpa consequuntur nam, aliquid, vel
                error adipisci fuga porro quam exercitationem ea amet eligendi nemo. A vero nesciunt blanditiis debitis aliquam ipsa, neque expedita ex
                necessitatibus doloremque quaerat deleniti, architecto dignissimos natus deserunt sunt? Minima ipsam ullam fuga eaque. Incidunt illo hic
                sapiente ullam cumque distinctio? Accusantium, magnam laborum exercitationem officia commodi eaque esse, cumque sequi blanditiis, animi ipsam
                reiciendis accusamus! Molestiae quos quo repudiandae quidem. Fugit, rem! Provident tempora, ex eveniet commodi, cumque eum nam consectetur
                deserunt, totam impedit mollitia cupiditate nesciunt vitae magnam ab ipsam doloremque. Omnis repudiandae incidunt obcaecati doloribus quia?
                Totam explicabo quos odio saepe voluptatum illum dolorum animi, facere dolor a reiciendis excepturi necessitatibus commodi voluptatibus aperiam
                temporibus quidem repudiandae perspiciatis. Reprehenderit minus autem vitae porro sunt placeat pariatur rerum omnis soluta dicta, itaque
                adipisci quod unde eveniet nemo iure laborum deserunt nesciunt illo distinctio expedita labore debitis consequatur. Inventore ex sunt
                dignissimos fugiat quidem saepe eius odio, nostrum libero iure modi, repellat vel excepturi pariatur mollitia! Perferendis voluptatum illum
                quibusdam dicta quae? Odit nisi sapiente impedit nam voluptas harum alias labore repudiandae quas ipsa sunt suscipit hic, voluptate laudantium
                nostrum eos inventore earum officiis, odio explicabo doloremque. Harum sapiente, laudantium officiis ut mollitia recusandae atque, laboriosam
                quam excepturi consequuntur omnis neque dolores ex! Iste repudiandae quae deleniti dolor neque minus quis sunt vitae perspiciatis, iusto, odio
                perferendis atque aspernatur, iure accusantium ab beatae ipsum velit qui consequatur quisquam. Similique quos velit beatae doloribus soluta,
                fugit ut atque quis tempore unde dolores, ullam nihil, veniam omnis vel in vero iure laudantium sequi sapiente eaque? Assumenda expedita
                nesciunt provident eos laboriosam? Nulla dolore incidunt praesentium mollitia sed repellendus autem provident tempora, a nostrum quam officiis
                magni dolorum repudiandae dolores ratione quasi, quibusdam illo eos error in dicta perspiciatis! Delectus magnam aperiam iure vero, sit amet
                accusamus excepturi ullam
            </p>
        </div>
    );
};
