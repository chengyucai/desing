import Divider from '@packages/core/src/Divider/index';

import * as React from 'react';
import { text, select, boolean } from '@storybook/addon-knobs';

// import * as cusClass from './custom.scss';

export default {
    title: 'Common(通用類)|Divider',
    parameters: {
        component: Divider,
        includeStories: [],
    },
};

export const Text = () => {
    const props = {
        word: text('word', 'Text'),
        orientation: select('orientation', ['bottom', 'top', 'left', 'right', 'default'], 'default'),
        vertical: boolean('vertical', false),
    };
    return (
        <div>
            <Divider {...props} />
        </div>
    );
};

export const Line = () => {
    const props = {};
    return (
        <div>
            <Divider {...props} />
        </div>
    );
};
export const Left = () => {
    return (
        <div>
            <Divider word={'Text Left'} orientation={'left'} />
        </div>
    );
};
export const Right = () => {
    return (
        <div>
            <Divider word={'Text Right'} orientation={'right'} />
        </div>
    );
};
export const Vertical = () => {
    return (
        <div>
            <Divider vertical />
        </div>
    );
};
export const Top = () => {
    return (
        <div>
            <Divider vertical word={'Vertical Text Top'} orientation={'top'} />
        </div>
    );
};
export const Bottom = () => {
    return (
        <div>
            <Divider vertical word={'我是中文'} orientation={'bottom'} />
        </div>
    );
};
