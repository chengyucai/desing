import ClickOutside from '@packages/core/src/ClickOutside/index';
import * as React from 'react';

export default {
    title: 'Common(通用類)|ClickOutside',
    parameters: {
        component: ClickOutside,
        includeStories: [],
    },
};

export const common = () => {
    return (
        <div style={{ height: '150px' }}>
            <ClickOutside>
                <ClickOutside.Trigger toggle>
                    <button>Hello</button>
                </ClickOutside.Trigger>
                <ClickOutside.Window close>World</ClickOutside.Window>
            </ClickOutside>
        </div>
    );
};

// export const simple = () => {
//     // const ref: any = React.useRef();

//     return (
//         <div style={{ height: '150px' }}>
//             <button
//                 onClick={() => {
//                     ref.current.toggle(true);
//                 }}
//             >
//                 Open
//             </button>
//             <ClickOutside ref={ref} defaultOpen>
//                 <span>Hello</span>
//             </ClickOutside>
//             暫時無法使用
//         </div>
//     );
// };

export const Dtm_Rajw = () => {
    const [inputValue, setinputValue] = React.useState('');

    return (
        <div style={{ height: '150px' }}>
            城市:
            <ClickOutside>
                <ClickOutside.Trigger>
                    <input
                        type="text"
                        value={inputValue}
                        onChange={e => {
                            setinputValue(e.target.value);
                        }}
                    />
                </ClickOutside.Trigger>
                {inputValue ? (
                    <ClickOutside.Window>
                        <div
                            onClick={() => {
                                setinputValue('');
                            }}
                        >
                            RAJN
                        </div>
                    </ClickOutside.Window>
                ) : (
                    <ClickOutside.Window closeBtn>
                        <div>DTM</div>
                    </ClickOutside.Window>
                )}
                (?)
            </ClickOutside>
        </div>
    );
};

export const scroll = () => {
    return (
        <div style={{ height: '1500px' }}>
            <ClickOutside scrollClose>
                <ClickOutside.Trigger toggle>
                    <button>Hello</button>
                </ClickOutside.Trigger>
                <ClickOutside.Window close>World</ClickOutside.Window>
            </ClickOutside>
        </div>
    );
};

export const exclude = () => {
    return (
        <div style={{ height: '1500px' }}>
            <ClickOutside scrollClose>
                <ClickOutside.Trigger toggle>
                    <button>click input</button>
                </ClickOutside.Trigger>
                <ClickOutside.Window exclude={['input']} close>
                    <div>這裡可以關閉</div>
                    <input defaultValue="不會關閉" />
                    <div>這裡可以關閉</div>
                </ClickOutside.Window>
            </ClickOutside>
        </div>
    );
};
