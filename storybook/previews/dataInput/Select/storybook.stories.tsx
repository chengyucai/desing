import Select from '@packages/core/src/Select';

import * as React from 'react';
import * as style from './custom.scss';

export default {
    title: 'DataInput(資料輸入類)|Select',
    parameters: {
        component: Select,
        includeStories: [],
    },
};

export const Common = () => {
    const [number, setnumber] = React.useState(3);
    const props = {
        data: [
            { text: 'apple', value: 1 },
            { text: 'banana', value: 2 },
            { text: 'gogogo', value: 3 },
        ],
        onChange: value => console.log(Number(value)),
        defValue: 1,
    };
    return (
        <div className={style.custom}>
            <Select {...props} />
        </div>
    );
};

export const All = () => {
    const props = {
        data: [
            { text: 'apple', value: 1 },
            { text: 'banana', value: 2 },
            { text: 'gogogo', value: 3 },
        ],
        onChange: value => console.log(Number(value)),
    };
    return (
        <div className={style.custom}>
            <Select {...props} all />
        </div>
    );
};

export const AllChange = () => {
    const props = {
        data: [
            { text: 'apple', value: 1 },
            { text: 'banana', value: 2 },
            { text: 'gogogo', value: 3 },
        ],
        onChange: value => console.log(value, Number(value)),
    };
    return (
        <div className={style.custom}>
            <Select {...props} all allChange={() => console.log('全部')} />
        </div>
    );
};
