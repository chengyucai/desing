import Drawer from '@packages/core/src/Drawer/index';
import Button from '@packages/core/src/Button';

import * as React from 'react';

import { text, select, boolean } from '@storybook/addon-knobs';

import * as style from './custom.scss';

export default {
    title: 'Feedback(反饋類)|Drawer',
    parameters: {
        component: Drawer,
        includeStories: [],
    },
};

export const Common = () => {
    const [visible, setvisible] = React.useState(false);
    const props = {
        placement: select('placement', ['top', 'right', 'bottom', 'left'], 'right'),
        title: text('title', 'Hello World'),
        closable: boolean('closable', true),
    };
    return (
        <div className={style.custom}>
            <Button type="xin" onClick={() => setvisible(true)}>
                Open
            </Button>
            <Drawer visible={visible} onClose={() => setvisible(false)} {...props}>
                <div>111</div>
                <div>2222</div>
                <div>44444</div>
            </Drawer>
        </div>
    );
};
