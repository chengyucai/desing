import Progress from '@packages/core/src/Progress/index';

import * as React from 'react';

import { select, number } from '@storybook/addon-knobs';

import * as style from './custom.scss';

export default {
    title: 'Feedback(反饋類)|Progress',
    parameters: {
        component: Progress,
        includeStories: [],
    },
};

export const Common = () => {
    const props = {
        percent: number('percent', 0),
        type: select('type', ['line', 'circle', 'dashboard'], 'line'),
        status: select('status', ['active', 'exception'], 'active'),
    };

    return (
        <div className={style.custom}>
            <Progress {...props} />
        </div>
    );
};
