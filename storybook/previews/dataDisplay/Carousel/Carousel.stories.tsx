import Carousel from '@packages/core/src/Carousel/index';

import * as React from 'react';

import { select, boolean } from '@storybook/addon-knobs';

// import * as cusClass from './custom.scss';

export default {
    title: 'DataDisplay(資料顯示類)|Carousel',
    parameters: {
        component: Carousel,
        includeStories: [],
    },
};

export const common = () => {
    const urls = [
        { path: 'https://fakeimg.pl/400x100/' },
        { path: 'https://fakeimg.pl/600x150/' },
        { path: 'https://fakeimg.pl/800x200/' },
        { path: 'https://fakeimg.pl/1000x250/' },
        { path: 'https://fakeimg.pl/1200x300/' },
    ];

    const onSlick = boolean('onSlick', false);

    const options = {
        '2 / 1': 2 / 1,
        '4 / 1': 4 / 1,
        '4 / 3': 4 / 3,
        '16 / 9': 16 / 9,
    };
    const ratio = select('ratio', options, 4 / 1);

    return (
        <div style={{ margin: '10px' }}>
            <Carousel urls={urls} timer={3000} onSlick={onSlick} ratio={ratio} />
        </div>
    );
};
