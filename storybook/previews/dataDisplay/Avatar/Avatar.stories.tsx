import Avatar from '@packages/core/src/Avatar/index';
import Badge from '@packages/core/src/Badge/index';

import * as React from 'react';

import { text, select, number } from '@storybook/addon-knobs';

// import * as cusClass from './custom.scss';

export default {
    title: 'DataDisplay(資料顯示類)|Avatar',
    parameters: {
        component: Avatar,
        includeStories: [],
    },
};

export const common = () => {
    const props = {
        shape: select('shape', ['circle', 'square'], 'square'),
        size: number('size', 48),
        src: text('src', 'https://images.cakeresume.com/cwchang93/bc31a60a-d1e3-4950-884b-1117b59d9edb.png'),
        alt: text('alt', '無效'),
        title: text('title', 'JW'),
    };

    return (
        <div style={{ margin: '10px' }}>
            <Badge count={9}>
                <Avatar {...props} />
            </Badge>
        </div>
    );
};

export const circle = () => {
    const props = {
        size: 48,
        src: 'https://images.cakeresume.com/cwchang93/bc31a60a-d1e3-4950-884b-1117b59d9edb.png',
        title: 'JW',
    };

    return (
        <div style={{ margin: '10px' }}>
            <Avatar {...props} shape={'circle'} />
        </div>
    );
};
export const NO_src = () => {
    const props = {
        size: 48,
        // title: 'JW',
    };

    return (
        <div style={{ margin: '10px' }}>
            <Avatar {...props} />
        </div>
    );
};
