import Badge from '@packages/core/src/Badge/index';
import * as React from 'react';

import { text, boolean, number } from '@storybook/addon-knobs';

import * as cusClass from './custom.scss';

export default {
    title: 'DataDisplay(資料顯示類)|Badge',
    parameters: {
        component: Badge,
        includeStories: [],
    },
};

export const common = () => {
    const props = {
        count: number('count', 5),
        overflowCount: number('overflowCount', 9),
        font: number('font', 12),
        dot: boolean('dot', false),
        showZero: boolean('showZero', false),
        color: text('color', '#52c41a'),
    };

    return (
        <div style={{ margin: '10px' }}>
            <Badge {...props}>
                <a href="#" className={cusClass.aLink} />
            </Badge>
        </div>
    );
};
