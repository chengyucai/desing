import BackTop from '@packages/core/src/BackTop/index';

import * as React from 'react';

// import * as cusClass from './custom.scss';

export default {
    title: 'navigate(頁面導航類)|BackTop',
    parameters: {
        component: BackTop,
        includeStories: [],
    },
};

export const Common = () => {
    const props = {};

    return (
        <div style={{ height: '200vh' }}>
            請拉至最下面
            <BackTop {...props} />
        </div>
    );
};
